#!/bin/bash
##########################################################################
# Copyright KAZ 2021							 #
# 									 #
# contact (at) kaz.bzh							 #
# 									 #
# This software is a filter to shrink email by attachment extraction.	 #
# 									 #
# This software is governed by the CeCILL-B license under French law and #
# abiding by  the rules of distribution  of free software. You  can use, #
# modify  and/or  redistribute  the  software under  the  terms  of  the #
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following #
# URL "http://www.cecill.info".						 #
# 									 #
# As a counterpart to the access to  the source code and rights to copy, #
# modify and  redistribute granted  by the  license, users  are provided #
# only with a limited warranty and  the software's author, the holder of #
# the economic  rights, and the  successive licensors have  only limited #
# liability.								 #
# 									 #
# In this respect, the user's attention is drawn to the risks associated #
# with loading,  using, modifying  and/or developing or  reproducing the #
# software by the user in light of its specific status of free software, #
# that may  mean that  it is  complicated to  manipulate, and  that also #
# therefore means  that it  is reserved  for developers  and experienced #
# professionals having in-depth computer  knowledge. Users are therefore #
# encouraged  to load  and test  the software's  suitability as  regards #
# their  requirements  in  conditions  enabling the  security  of  their #
# systems and/or  data to  be ensured  and, more  generally, to  use and #
# operate it in the same conditions as regards security.		 #
# 									 #
# The fact that  you are presently reading this means  that you have had #
# knowledge of the CeCILL-B license and that you accept its terms.	 #
##########################################################################

PRG=$(basename $0)

usage () {
    echo "Usage: ${PRG} logDir"
    exit 1
}

BOLD='\e[1m'
RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
BLUE='\e[0;34m'
MAGENTA='\e[0;35m'
CYAN='\e[0;36m'
NC='\e[0m' # No Color
NL='
'
[ "$#" -eq 1 ] || usage
logDir=$(realpath "$1")

########################################
# recherche des binaires
cd $(dirname $0)
eMailShrinker="$(realpath "./eMailShrinker")"
[ -x "${eMailShrinker}" ] || eMailShrinker="$(realpath "../../build/out/eMailShrinker")"
[ -x "${eMailShrinker}" ] || ( echo "${RED}eMailShrinker not found${NC}" ; exit)


cd "${logDir}"
for i in in.*.orig;
do
    clear
    echo -e "          ${GREEN}${BOLD}${i//.orig}${NC}\n"
    "${eMailShrinker}" -l ${i}
    echo -e "\n          ####################\n"
    "${eMailShrinker}" -l ${i//.orig}.altered
    echo -en "\n(q = quit / y = rm / default = continue)? "
    read rep
    case "${rep}" in
	"q" ) break;;
	"y") rm ${i} ${i//.orig}.altered
    esac
done
