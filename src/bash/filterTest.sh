#!/bin/bash
##########################################################################
# Copyright KAZ 2021							 #
# 									 #
# contact (at) kaz.bzh							 #
# 									 #
# This software is a filter to shrink email by attachment extraction.	 #
# 									 #
# This software is governed by the CeCILL-B license under French law and #
# abiding by  the rules of distribution  of free software. You  can use, #
# modify  and/or  redistribute  the  software under  the  terms  of  the #
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following #
# URL "http://www.cecill.info".						 #
# 									 #
# As a counterpart to the access to  the source code and rights to copy, #
# modify and  redistribute granted  by the  license, users  are provided #
# only with a limited warranty and  the software's author, the holder of #
# the economic  rights, and the  successive licensors have  only limited #
# liability.								 #
# 									 #
# In this respect, the user's attention is drawn to the risks associated #
# with loading,  using, modifying  and/or developing or  reproducing the #
# software by the user in light of its specific status of free software, #
# that may  mean that  it is  complicated to  manipulate, and  that also #
# therefore means  that it  is reserved  for developers  and experienced #
# professionals having in-depth computer  knowledge. Users are therefore #
# encouraged  to load  and test  the software's  suitability as  regards #
# their  requirements  in  conditions  enabling the  security  of  their #
# systems and/or  data to  be ensured  and, more  generally, to  use and #
# operate it in the same conditions as regards security.		 #
# 									 #
# The fact that  you are presently reading this means  that you have had #
# knowledge of the CeCILL-B license and that you accept its terms.	 #
##########################################################################
#  Kaz addon (see https://git.kaz.bzh/KAZ/depollueur for information)
#  version : 2.24 (2025-01-26)

PRG=$(basename $0)

ATTACH_MODE="FOOTER"
export SIMULATE=""

BOLD='\e[1m'
RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
BLUE='\e[0;34m'
MAGENTA='\e[0;35m'
CYAN='\e[0;36m'
NC='\e[0m' # No Color
NL='
'

PERIODE="month"
MD5_CMD=/usr/bin/md5sum
########################################
LOG () {
    echo -e "$1" 1>&2
}

usage () {
    echo "Usage: ${PRG} [-h|-v|-g] [-m {NONE|FOOTER|ATTACHMENT|BOTH}] mbox"
    exit 1
}

while : ; do
    case "$1" in
	-h*) usage;;
	-v*) "${eMailShrinker}" -v; exit;;
	-g) DEBUG="-g"; shift;;
	-s) SIMULATE="echo"; shift;;
	-m) shift; ATTACH_MODE="$1"; shift;;
	*) break;;
    esac
done
case "${ATTACH_MODE}" in
    ""|NONE|FOOTER|ATTACHMENT|BOTH);;
    *) usage;;
esac

[ -z "${ATTACH_MODE}" ] || ATTACH_MODE="-m ${ATTACH_MODE}"

[ "$#" -eq 1 ] || usage
mbox=$(realpath "$1")

########################################
# recherche des binaires
cd $(dirname $0)
eMailShrinker="$(realpath "./eMailShrinker")"
[ -x "${eMailShrinker}" ] || eMailShrinker="$(realpath "../../build/out/eMailShrinker")"
[ -x "${eMailShrinker}" ] || ( echo -e "${RED}eMailShrinker not found${NC}" ; exit)

########################################
dos2unix "${mbox}"
[ -z "${SIMULATE}" ] && DOMAINEDEPOT="$(cat config/domainedepot)"
JIRAFEAU_URL="https://depot.${DOMAINEDEPOT}"
JIRAFEAU_LOCAL="${JIRAFEAU_URL}"

TMP_DIR="$(mktemp)"

########################################
# curl Jirafeau
curlJirafeauUpdate () {
    # $1: periode
    # $2: jirafeauItemRef

    [ -z "${SIMULATE}" ] && curl -X POST -d "u=$1" -d "h=$2" "${JIRAFEAU_LOCAL}/a.php"
}

curlJirafeauSend () {
    # $1: periode
    # $2: filename
    # $3: content-type
    # $4: name
    # $5: password

    if [ -n "${SIMULATE}" ]; then
	echo -e "TokenXXX\nCodeX"
	return
    fi
    type="type=$3;"
    [ -z "$3" -o "$3" = "/" ] && type=""
    LOG "curl -X POST -F \"time=$1\" -F \"key=$5\" -F \"file=@$2;${type}filename=\\\"$4\\\"\" \"${JIRAFEAU_LOCAL}/s.php\""
    curl -X POST -F "time=$1" -F "key=$5" -F "file=@$2;${type}filename=\"$4\"" "${JIRAFEAU_LOCAL}/s.php" || exit 1
}

########################################
# nettoyage
rm -f  "${TMP_DIR}" ; mkdir -p "${TMP_DIR}"
rm -fr "${TMP_DIR}/PJ-name.txt" "${TMP_DIR}/PJ-Keys.txt" "${TMP_DIR}/PJ" "${TMP_DIR}/archive-content.txt" "${TMP_DIR}/url-to-refresh.txt" "${TMP_DIR}/new-mbox"

echo -e "time: $(date "+%Y-%m-%d-%H:%M:%S")\nid: $(date +%s)" > "${TMP_DIR}/archive-content.txt"

########################################
# affichage de la structure de départ
LOG "  - ${BLUE}mbox: ${mbox}${NC}${YELLOW}"
"${eMailShrinker}" -l "${mbox}"
LOG "${NC}"

########################################
# Préparation pour le cloud
rm -f "${TMP_DIR}/last.txt"

########################################
# recherche des prolongations des délais de grace
"${eMailShrinker}" ${DEBUG} -u "${mbox}" > "${TMP_DIR}/url-to-refresh.txt"
cat "${TMP_DIR}/url-to-refresh.txt" | grep "${JIRAFEAU_URL}" | while read REMOTE_LINK; do
    REMOTE_REF=$(echo "${REMOTE_LINK}" | sed -e 's/.*h=\([^&]*\).*/\1/' -e 's/.*http.*//')
    [ -z "${REMOTE_REF}" ] && continue
    LOG "  - ${BLUE}update ${REMOTE_REF}${NC}"
    curlJirafeauUpdate "${PERIODE}" "${REMOTE_REF}"
    echo "old: ${REMOTE_REF} ${REMOTE_KEY}" >> "${TMP_DIR}/archive-content.txt"
    echo "h=${REMOTE_REF}~${REMOTE_KEY}" > "${TMP_DIR}/last.txt"
done

########################################
# extraction des pièces jointes
"${eMailShrinker}" ${DEBUG} -s "5ki" -d "${TMP_DIR}/PJ" "${mbox}" > "${TMP_DIR}/PJ-name.txt" || exit 1

LOG "  - ${BLUE}PJ-name: ${NC}"
cat "${TMP_DIR}/PJ-name.txt"
LOG

########################################
# dépot des extractions dans jirafeau et récupération des codes
cat "${TMP_DIR}/PJ-name.txt" | {
    while read ATTACH_TMP_NAME; do
	LOG " - ${BLUE}find ATTACH_TMP_NAME: (${ATTACH_TMP_NAME}) ${NC}"
	if [ -d "${ATTACH_TMP_NAME}" ]; then
	    ATTACH_MEDIA="${ATTACH_TMP_NAME}/media"
	    ATTACH_NAME=$(grep "^Name: " "${ATTACH_TMP_NAME}/meta" | cut -c 7- )
	    ATTACH_CONTENT_TYPE=$(grep "^Content-Type: " "${ATTACH_TMP_NAME}/meta" | cut -c 15- )
	else
	    LOG " - ${RED}no ATTACH_TMP_NAME: ${ATTACH_TMP_NAME}${NC}"
	    # XXX error
	    continue
	fi
	LOG " - ${BLUE}find ${ATTACH_NAME} / (${ATTACH_CONTENT_TYPE}) / ${ATTACH_MEDIA} ${NC}"
	PASSWORD=$(apg -n 1 -m 12 -M cln)
	PASSWORD_MD5=$(echo -n ${PASSWORD} | ${MD5_CMD} | cut -d \  -f 1)
	curlJirafeauSend "${PERIODE}" "${ATTACH_MEDIA}" "${ATTACH_CONTENT_TYPE}" "${ATTACH_NAME}" "${PASSWORD}" > "${TMP_DIR}/one.txt"

	cat "${TMP_DIR}/one.txt"  | {
	    read JIR_TOKEN
	    read JIR_CODE
	    case "${JIR_TOKEN}" in
		"" | no | *Error* | \<* )
		    LOG "  - ${RED}can't upload ${ATTACH_MEDIA} <${JIR_TOKEN}> <${JIR_CODE}>${NC}"
		    cat "${TMP_DIR}/one.txt" >&2
		    echo "url:"
		    exit 1
		    ;;
		* )
		    LOG "  - ${GREEN} upload ${ATTACH_MEDIA}${NC}"
		    echo "url: ${JIRAFEAU_URL}/f.php?d=1&h=${JIR_TOKEN}&k=${PASSWORD_MD5}"
		    echo "new: ${JIR_TOKEN} ${PASSWORD_MD5}" >> "${TMP_DIR}/archive-content.txt"
		    echo "h=${JIR_TOKEN}~${PASSWORD_MD5}" > "${TMP_DIR}/last.txt"
		    ;;
	    esac
	}
    done
    NB_ATTACH=$(grep -e "^old: " -e "^new: " "${TMP_DIR}/archive-content.txt" | wc -l)
    if [ "${NB_ATTACH}" -gt 1 ]; then
	PASSWORD=$(apg -n 1 -m 12 -M cln)
	PASSWORD_MD5=$(echo -n ${PASSWORD} | ${MD5_CMD} | cut -d \  -f 1)
	curlJirafeauSend "${PERIODE}" "${TMP_DIR}/archive-content.txt" "text/kaz_email_archive" "archive_content" "${PASSWORD}" > "${TMP_DIR}/one.txt" || exit 1
	cat "${TMP_DIR}/one.txt"  | {
	    read JIR_TOKEN
	    read JIR_CODE
	    case "${JIR_TOKEN}" in
		"" | no | *Error* | \<* )
		    LOG "  - ${RED}can't upload ${TMP_DIR}/archive-content.txt${NC}"
		    echo "arch: bad"
		    exit 1
		    ;;
		* )
		    LOG "  - ${GREEN} upload archive-content.txt${NC}"
		    echo "arch: ${JIRAFEAU_URL}/a.php?g=${JIR_TOKEN}~${PASSWORD_MD5}"
		    echo "g=${JIR_TOKEN}~${PASSWORD_MD5}" > "${TMP_DIR}/last.txt"
		    ;;
	    esac
	}
    else
	LOG "  - ${GREEN}no archive${NC}"
	echo "arch: none"
    fi
    if [ -s "${TMP_DIR}/last.txt" ]; then
	echo "cloud: ${JIRAFEAU_URL}/c.php?$(cat "${TMP_DIR}/last.txt")"
    else
	echo "cloud: none"
    fi

} > "${TMP_DIR}/PJ-Keys.txt"

LOG "  - ${BLUE}PJ-Keys: ${NC}"
cat "${TMP_DIR}/PJ-Keys.txt"
LOG

LOG "  - ${GREEN}ATTACH_MODE: ${ATTACH_MODE}${NC}"

########################################
# substitution des pièces jointes par les codes fournis par jirafeau
cat "${TMP_DIR}/PJ-Keys.txt" | "${eMailShrinker}" ${DEBUG} ${ATTACH_MODE} -s "5ki" "${mbox}" "${TMP_DIR}/new-mbox" || exit 1

########################################
# affichage de la structure à la fin
LOG "  - ${BLUE}new-mbox:${NC}${YELLOW}"
"${eMailShrinker}" -l "${TMP_DIR}/new-mbox" || ( LOG "${NC}" ; exit 1)

echo -e "\n${NC}${GREEN}${BOLD}resul in ${TMP_DIR}/new-mbox${NC}"
exit 0
