#!/bin/bash
##########################################################################
# Copyright KAZ 2021							 #
# 									 #
# contact (at) kaz.bzh							 #
# 									 #
# This software is a filter to shrink email by attachment extraction.	 #
# 									 #
# This software is governed by the CeCILL-B license under French law and #
# abiding by  the rules of distribution  of free software. You  can use, #
# modify  and/or  redistribute  the  software under  the  terms  of  the #
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following #
# URL "http://www.cecill.info".						 #
# 									 #
# As a counterpart to the access to  the source code and rights to copy, #
# modify and  redistribute granted  by the  license, users  are provided #
# only with a limited warranty and  the software's author, the holder of #
# the economic  rights, and the  successive licensors have  only limited #
# liability.								 #
# 									 #
# In this respect, the user's attention is drawn to the risks associated #
# with loading,  using, modifying  and/or developing or  reproducing the #
# software by the user in light of its specific status of free software, #
# that may  mean that  it is  complicated to  manipulate, and  that also #
# therefore means  that it  is reserved  for developers  and experienced #
# professionals having in-depth computer  knowledge. Users are therefore #
# encouraged  to load  and test  the software's  suitability as  regards #
# their  requirements  in  conditions  enabling the  security  of  their #
# systems and/or  data to  be ensured  and, more  generally, to  use and #
# operate it in the same conditions as regards security.		 #
# 									 #
# The fact that  you are presently reading this means  that you have had #
# knowledge of the CeCILL-B license and that you accept its terms.	 #
##########################################################################

PRG=$(basename $0)
FILTER_TEST="$(realpath "$(dirname $0)/filterTest.sh")"

ATTACH_MODE="FOOTER"

usage () {
    echo "Usage: ${PRG} {NONE|FOOTER|ATTACHMENT|BOTH} mbox..."
    exit 1
}

########################################
BOLD='\e[1m'
RED='\e[0;31m'
GREEN='\e[0;32m'
YELLOW='\e[0;33m'
BLUE='\e[0;34m'
MAGENTA='\e[0;35m'
CYAN='\e[0;36m'
GREY='\e[0;90m'
BG_BLACK='\e[0;40m'
BG_RED='\e[0;41m'
BG_GREEN='\e[0;42m'
BG_YELLOW='\e[0;43m'
BG_BLUE='\e[0;44m'
BG_MAGENTA='\e[0;45m'
CYAN='\e[0;36m'
NC='\e[0m' # No Color
NL='
'

########################################
[ "$#" -lt 2 ] && usage

ATTACH_MODE="$1"; shift

case "${ATTACH_MODE}" in
    ""|NONE|FOOTER|ATTACHMENT|BOTH);;
    *) usage;;
esac


start=0
loop=true
while [ ! -z "${loop}" ]; do
    loop=""
    count=0
    for mbox in $*; do
	clear
	((count=count+1))
	(( count < start )) && continue
	echo -e "          ${GREY}${count}/$# ${GREEN}${BOLD}${mbox}${NC}\n"
	"${FILTER_TEST}" -s -m "${ATTACH_MODE}" "${mbox}"
	echo -en "\n(q = quit / [0-9]* = goto / default = continue)? "
	read rep
	case "${rep}" in
	    "q" ) break;;
	    [0-9]* )
		start="${rep}"
		(( count < start )) && continue
		loop=true
		break;;
	esac
    done
done
