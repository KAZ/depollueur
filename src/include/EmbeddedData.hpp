////////////////////////////////////////////////////////////////////////////
// Copyright KAZ 2021							  //
// 									  //
// contact (at) kaz.bzh							  //
// 									  //
// This software is a filter to shrink email by attachment extraction.	  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _kaz_EmbeddedData_hpp
#define _kaz_EmbeddedData_hpp

#include <string>
#include <vector>

namespace kaz {

  using namespace std;

  // ================================================================================
  /*! properties of embedded image in html part (rfc2397) */
  class EmbeddedData {
  public:
    /*! rank of this image tag */
    int imgIdx;
    /*! extracted in first pass */
    string contentType, name;
    string downloadUrl;
    /*! area of base64 relative in the image section */
    string::size_type startData, dataLength;

    /*! initialisation in the first pass */
    EmbeddedData (const int &imgIdx, const string &contentType, const string &name, const string::size_type &startData, const string::size_type &dataLength);

    /*! records properties */
    static void fillEmbeddedData (const vector<string> &imgs, const streamoff &minAttachSize, vector<EmbeddedData> &data);

    // friend ostream& operator << (ostream& os, const EmbeddedData& embeddedData);
  };
  ostream& operator << (ostream& os, const EmbeddedData& embeddedData);

  // ================================================================================
}

#endif // _kaz_EmbeddedData_hpp
