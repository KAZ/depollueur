////////////////////////////////////////////////////////////////////////////
// Copyright KAZ 2021							  //
// 									  //
// contact (at) kaz.bzh							  //
// 									  //
// This software is a filter to shrink email by attachment extraction.	  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _kaz_SizeArg_hpp
#define _kaz_SizeArg_hpp

#include <iostream>
#include <string>
#include <boost/lexical_cast.hpp>

namespace kaz {

  using namespace std;

  // ================================================================================
  /*! human readable of size values */
  class SizeArg {
  private:
    /*! the size */
    size_t bytes;

    /*! human readable convertion  */
    void init (const string &option);
  public:
    /*! scalar convertion  */
    operator size_t () const { return bytes; }

    /*! initialization from scalar value */
    SizeArg (const size_t &bytes = 0);
    /*! initialization from human readable value */
    SizeArg (const string &option);

    friend ostream &operator << (ostream &out, const SizeArg &sizeArg);
    friend istream &operator >> (istream &in, SizeArg &sizeArg);
  };

  // ================================================================================
  /*! human readable convertion */
  ostream &operator << (ostream &out, const SizeArg &sizeArg);
  istream &operator >> (istream &in, SizeArg &sizeArg);
}

#endif // _kaz_Attachment_hpp
