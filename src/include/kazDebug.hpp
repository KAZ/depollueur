////////////////////////////////////////////////////////////////////////////
// Copyright KAZ 2021							  //
// 									  //
// contact (at) kaz.bzh							  //
// 									  //
// This software is a filter to shrink email by attachment extraction.	  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _Kaz_Debug_hpp
#define _Kaz_Debug_hpp

#include <iostream>
#include <string>

/*! log error */
#define LOG_BUG(cond, action, expr) {if (cond) {std::cerr << endl << expr << std::endl << std::flush; action; }}

#ifdef ENABLE_SMART_LOG

#ifndef SMART_DEF_LOG
#define SMART_DEF_LOG(name, expr) DEF_LOG (name, expr)
#endif

#ifndef SMART_LOG
#define SMART_LOG(expr) LOG (expr)
#endif

#ifndef SMART_LOG_EXPR
#define SMART_LOG_EXPR(expr) {if (::kaz::Log::debug) {expr;} }
#endif

#else

#ifndef SMART_DEF_LOG
#define SMART_DEF_LOG(name, expr)
#endif

#ifndef SMART_LOG
#define SMART_LOG(expr)
#endif

#ifndef SMART_LOG_EXPR
#define SMART_LOG_EXPR(expr)
#endif
#endif

#ifdef DISABLE_LOG

#ifndef DEF_LOG
#define DEF_LOG(name, expr)
#endif
#ifndef LOG
#define LOG(expr) {}
#endif

#ifndef DEBUG
#define DEBUG(expr) {}
#endif

#else

#ifndef DEF_LOG
/*! to placed as the first instruction to log entry and return method */
#define DEF_LOG(name, expr) ::kaz::Log log (name); { if (::kaz::Log::debug) std::cerr << expr << std::endl << std::flush; }
#endif

#ifndef LOG
/*! to placed in methode where DEF_LOG if call previously */
// _______________________________________________________ Don't forget DEF_LOG
#define LOG(expr) { if (::kaz::Log::debug) std::cerr << log << "| " << expr << std::endl << std::flush; }
#endif

#ifndef DEBUG
/*! log without format */
#define DEBUG(expr) { if (::kaz::Log::debug) std::cerr << expr << std::endl << std::flush; }
#endif

#endif

namespace kaz {
  // ================================================================================
  using namespace std;

  /*! manage prety print log */
  class Log {
    /*! visual indentation of call */
    static size_t indent;
    /*! name recall in log */
    string functName;
  public:
    /*! switch on the log */
    static bool debug;

    /*! log entry of a method */
    Log (const string &functName);
    /*! log return of a method */
    ~Log ();

    /*! timestamp of the log */
    static string getLocalTimeStr ();
    friend ostream &operator << (ostream &out, const Log &log);
  };
  ostream &operator << (ostream &out, const Log &log);

  // ================================================================================
} // kaz

#endif //_Kaz_Debug_hpp
