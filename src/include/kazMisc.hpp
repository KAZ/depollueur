////////////////////////////////////////////////////////////////////////////
// Copyright KAZ 2021							  //
// 									  //
// contact (at) kaz.bzh							  //
// 									  //
// This software is a filter to shrink email by attachment extraction.	  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#ifndef _kaz_misc_hpp
#define _kaz_misc_hpp

#include <string>
#include <ctype.h>
#include <map>
#include <regex>

namespace kaz {
  using namespace std;

  // =======================================================================
  /*! ordered base64 chars */
  extern const char * const base64Chars;
  /*! set of chars available in URL */
  extern const string availableURLChars;
  /*! pattern for encoded words */
  extern const regex encodedWordRegex;

  // =======================================================================
  /*! get the width of the terminal */
  uint16_t getCols ();

  /*! display time. */
  string ns2string (const double &delta);

  // =======================================================================
  /*! side effect on str to replace "from" by "to" */
  void replaceAll (string& str, const string &from, const string &to);
  /*! side effect on str to replace a set of "from" by a set of "to" */
  void replaceAll (string& str, const map<const string, const string> &subst);

  // =======================================================================
  /*! side effect to lower case a string (in mime section) */
  void toLower (string &content);

  /*! compare strings are done in uppercase to avoid accents. Give token in uppercase spin up the process */
  const string &toUpperIfNeed (const string &src, string &tmp);
  /*! find upper case of p in upper case of s */
  string::size_type caseInsensitiveFind (const string& s, const string& p, const string::size_type &pos = 0);
  /*! reverse find upper case of p in upper case of s */
  string::size_type caseInsensitiveRFind (const string& s, const string& p, const string::size_type &pos = 0);

  string boundaryGen (const int &size);

  /*! side effect to repplace =XX by the char with de haxe value XX. It could be %XX in rfc2184 */
  template<char delim='='>
  void quotedDecode (string &content);
  /*! side effect to quoted-printable content rfc2045 */
  void quotedEncode (string &content);
  /*! side effect to decode base64 */
  void base64Decode (string &content);
  /*! side effect to encode base64 */
  void base64Encode (string &content);
  /*! side effect to change charset of content */
  void iso2utf (string &content);
  /*! side effect to get the encoded word according rfc2047 rfc5987 rfc2978 */
  void encodedWordDecode (string &content);
  /*! side effect to get the charsetValue according rfc2184 */
  void charsetValueDecode (string &content);
  /*! side effect to remove quote */
  void removeQuote (string &content);

  // =======================================================================
  /*! return if the c need no quote */
  inline bool
  isQuotedPrintable (const char &c) {
    return
      c == ' ' || (c >= 33 && c <= 126 && c != '=' && c != '.');
    // '.' is available in rfc2184 but it avoid to check '.' alone in a line :-)
  }

  /*! return if the c is in available base64 chars */
  inline bool
  isBase64 (const char &c) {
    return (isalnum (c) || (c == '+') || (c == '/'));
  }

  /*! get the order of c in the base64 set of values */
  inline unsigned char
  getBase64Val (const char &c) {
    if (c == '+')
      return 62;
    if (c == '/')
      return 63;
    if (c <= '9')
      return (c-'0')+52;
    if (c <= 'Z')
      return (c-'A');
    return (c-'a')+26;
  }

  /*! get the nibble value of c representation of an hexa digit */
  inline unsigned char
  getHexaVal (const char &c) {
    if (c <= '9')
      return c-'0';
    if (c <= 'F')
      return (c-'A')+10;
    return (c-'a')+10;
  }

  /*! get the nibble value of c representation of an hexa digit */
  inline void
  getHexa (const char &c, char &upper, char &lower) {
    upper = c >> 4 & 0xF;
    upper += upper > 9 ? ('A'-10) : '0';
    lower = c & 0xF;
    lower += lower > 9 ? ('A'-10) : '0';
  }

  // =======================================================================
}

#endif // _kaz_Attachment_hpp
