////////////////////////////////////////////////////////////////////////////
// Copyright KAZ 2021							  //
// 									  //
// contact (at) kaz.bzh							  //
// 									  //
// This software is a filter to shrink email by attachment extraction.	  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include "kazDebug.hpp"
#include "kazMisc.hpp"
#include "EmbeddedData.hpp"
#include "Attachment.hpp"

using namespace std;
using namespace kaz;

// ================================================================================
static const string EMBEDDED_TAG ("SRC=\"DATA:");

// ================================================================================
EmbeddedData::EmbeddedData (const int &imgIdx, const string &contentType, const string &name, const string::size_type &startData, const string::size_type &dataLength)
  : imgIdx (imgIdx),
    contentType (contentType),
    name (name),
    startData (startData),
    dataLength (dataLength) {
  DEF_LOG ("EmbeddedData::EmbeddedData", "imgIdx: " << imgIdx << " contentType:" << contentType << " name:" << name << " startData:" << startData << " dataLength:" << dataLength);
}

// ================================================================================
void
EmbeddedData::fillEmbeddedData (const vector<string> &imgs, const streamoff &minAttachSize, vector<EmbeddedData> &data) {
  DEF_LOG ("EmbeddedData::fillEmbeddedData", "imgs.size: " << imgs.size () << " minAttachSize:" << minAttachSize << " data.size:" << data.size ());

  int imgIdx (-1);
  for (const string &img : imgs) {
    ++imgIdx;
    if (streamoff (img.length ()) < minAttachSize)
      continue;
    string::size_type startPos (caseInsensitiveFind (img, EMBEDDED_TAG));
    if (startPos == string::npos)
      continue;
    startPos += EMBEDDED_TAG.length ();
    // XXX check base64 ?
    string::size_type endPos = img.find_first_of (";,", startPos);

    LOG_BUG (endPos == string::npos, continue, "eMailShrinker: bug E1: can't find end of contentType" );
    const string contentType (img.substr (startPos, endPos-startPos));
    const string name (Attachment::getUnknown (contentType));
    startPos = img.find (',', startPos);

    LOG_BUG (startPos == string::npos, continue, "eMailShrinker: bug E2: can't find start data" );

    ++startPos;
    endPos = img.find ('"', startPos);
    data.push_back (EmbeddedData (imgIdx, contentType, name, startPos, endPos-startPos));
  }
}

// ================================================================================
ostream&
kaz::operator << (ostream& os, const EmbeddedData& embeddedData) {
  os << embeddedData.imgIdx << ": "
     << embeddedData.contentType << " - " << embeddedData.name
     << " (" << embeddedData.startData << " / " << embeddedData.dataLength << ") "
     << embeddedData.downloadUrl
     << endl;
  return os;
}

// ================================================================================
