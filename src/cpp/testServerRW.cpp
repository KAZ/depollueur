////////////////////////////////////////////////////////////////////////////
// Copyright KAZ 2021							  //
// 									  //
// contact (at) kaz.bzh							  //
// 									  //
// This software is a filter to shrink email by attachment extraction.	  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <arpa/inet.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <sys/socket.h>
#include <unistd.h>

#include "kazDebug.hpp"

using namespace std;
using namespace kaz;

const string LAST_VERSION_NUM ("3.0");
const string LAST_VERSION_DATE ("2024-01-01");
const string LAST_VERSION (LAST_VERSION_NUM+" "+LAST_VERSION_DATE+" testServerRW");

#define PORT 8080

int
main (int argc, char const* argv[]) {
  Log::debug = true;
  DEF_LOG ("main:", LAST_VERSION);

  int clientSocket;
  struct sockaddr_in serv_addr;
  LOG ("create socket");
  if ((clientSocket = socket (AF_INET, SOCK_STREAM, 0)) < 0) {
    cerr << endl << "Socket creation error" << endl;
    perror ("socket failed");
    return -1;
  }
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_port = htons (PORT);

  LOG ("check address");
  if (inet_pton (AF_INET, "127.0.0.1", &serv_addr.sin_addr) <= 0) {
    cerr << endl << "Invalid address / Address not supported" << endl;
    perror ("socket failed");
    return -1;
  }

  LOG ("connect on " << PORT);
  if (connect (clientSocket, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
    cerr << endl << "Connection Failed" << endl;
    perror ("socket failed");
    return -1;
  }

  bool end (false);
  LOG ("send original email (end with QUIT)");
  for (;;) {
    if (!end) {
      string hello;
      cin >> hello;
      if (hello == "QUIT") {
	shutdown (clientSocket, SHUT_WR);
	LOG ("shutdown send");
	break;
      }
      LOG ("avant");
      hello.push_back ('\n');
      send (clientSocket, hello.c_str (), hello.size (), 0);
      LOG ("apres");
    }
  }
  LOG ("receive...");
  for (;;) {
    char buffer[1024] = { 0 };
    ssize_t msgSize (recv (clientSocket, buffer, 1024 - 1, 0));
    switch (msgSize) {
    case -1 :
      perror ("socket failed");
      return -1;
    case 0 :
      LOG ("shutdown receive");
      close (clientSocket);
      return 0;     
      break;
    default:
      //cout.write (buffer, msgSize);
      printf ("%s\n", buffer);
    }
  }
}
