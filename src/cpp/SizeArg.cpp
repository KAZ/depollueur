////////////////////////////////////////////////////////////////////////////
// Copyright KAZ 2021							  //
// 									  //
// contact (at) kaz.bzh							  //
// 									  //
// This software is a filter to shrink email by attachment extraction.	  //
// 									  //
// This software is governed by the CeCILL-B license under French law and //
// abiding by  the rules of distribution  of free software. You  can use, //
// modify  and/or  redistribute  the  software under  the  terms  of  the //
// CeCILL-B license as circulated by CEA, CNRS and INRIA at the following //
// URL "http://www.cecill.info".					  //
// 									  //
// As a counterpart to the access to  the source code and rights to copy, //
// modify and  redistribute granted  by the  license, users  are provided //
// only with a limited warranty and  the software's author, the holder of //
// the economic  rights, and the  successive licensors have  only limited //
// liability.								  //
// 									  //
// In this respect, the user's attention is drawn to the risks associated //
// with loading,  using, modifying  and/or developing or  reproducing the //
// software by the user in light of its specific status of free software, //
// that may  mean that  it is  complicated to  manipulate, and  that also //
// therefore means  that it  is reserved  for developers  and experienced //
// professionals having in-depth computer  knowledge. Users are therefore //
// encouraged  to load  and test  the software's  suitability as  regards //
// their  requirements  in  conditions  enabling the  security  of  their //
// systems and/or  data to  be ensured  and, more  generally, to  use and //
// operate it in the same conditions as regards security.		  //
// 									  //
// The fact that  you are presently reading this means  that you have had //
// knowledge of the CeCILL-B license and that you accept its terms.	  //
////////////////////////////////////////////////////////////////////////////

#include <regex>

#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>

#include "kazDebug.hpp"
#include "SizeArg.hpp"

using namespace std;
using namespace kaz;

// ================================================================================
SizeArg::SizeArg (const size_t &bytes)
  : bytes (bytes) {
}

SizeArg::SizeArg (const string &option)
  : bytes (0) {
  init (option);
}

void
SizeArg::init (const string &token) {
  DEF_LOG ("SizeArg::init", "token: " << token);
  static const string prefix ("KMGTPEZY");
  static const regex formatRegEx ("([0-9]+) *([k"+prefix+"]?)(i?)");

  if (!regex_match (token.begin (), token.end (), formatRegEx))
    throw invalid_argument ("Bad size");
  bytes = boost::lexical_cast<uint64_t> (regex_replace (token, formatRegEx, "$1"));
  const string v2 (regex_replace (token, formatRegEx, "$2"));
  size_t index = prefix.find (v2);
  if (v2.length ()) {
    if (index == string::npos)
      index = 0; // "k" case
    ++index;
  }
  bytes *= pow (regex_replace (token, formatRegEx, "$3").empty () ? 1000 : 1024, index);
  LOG ("token:" << token << " index:" << index << " v2:<" << v2 << ">" << " b:" << bytes);
}


// ================================================================================
ostream &
kaz::operator << (ostream &out, const SizeArg &sizeArg) {
  static string sizes [] = {"", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi", "Yi"};

  if (!sizeArg.bytes)
    return out << "0 byte";
  int nbBytes = (int) floor (log (sizeArg.bytes) / log (1024));
  double val ((sizeArg.bytes / pow (1024, nbBytes)));
  return out << boost::str (boost::format(nbBytes ? "%.2f " : val == 1 ? "%.0f byte" : + "%.0f bytes") % val) + sizes [nbBytes];
}

istream &
kaz::operator >> (istream &in, SizeArg &sizeArg) {
  string token;
  in >> token;
  try {
    sizeArg.init (token);
  } catch (...) {
    in.setstate (ios_base::failbit);
  }
  return in;
}

// ================================================================================
