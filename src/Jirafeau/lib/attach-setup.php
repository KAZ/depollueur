<?php
/*
 *  Kaz addon (see https://git.kaz.bzh/KAZ/depollueur for information)
 *  commun function for a.php and c.php
 *  version : 2.24 (2025-01-26)
 */

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

define ('JIRAFEAU_ROOT', dirname (__FILE__) . '/../');

require (JIRAFEAU_ROOT . 'lib/settings.php');
require (JIRAFEAU_ROOT . 'lib/functions.php');
require (JIRAFEAU_ROOT . 'lib/lang.php');

// ========================================
// FILES
define ('VAR_TOKENS', $cfg ['var_root'].'tokens/');
define ('VAR_MODE', $cfg ['var_root'].'mode/');
define ('VAR_TRACKS', $cfg ['var_root'].'tracks/');
define ('VAR_PERIOD', $cfg ['var_root'].'period/');
define ('VAR_LANG', $cfg ['var_root'].'lang/');
define ('VAR_FAKE', $cfg ['var_root'].'fake/');
define ('VAR_ADMIN', $cfg ['var_root'].'admin/');
define ('VAR_CONFIG', $cfg ['var_root'].'config/');
define ('VAR_CLOUD', $cfg['var_root'].'cloud/'); 
define ('FILE_CONFIG', VAR_CONFIG.'default.php');

$domain="kaz.local";
if (preg_match ("%^.*//([^/]*)/?.*$%", $cfg ['web_root'], $matches)) {
    $domain = $matches [1];
}
if (preg_match ("%^depot\.(.*)$%", $domain, $matches)) {
    $domain = $matches [1];
}

// XXX
$DEFAULT_CLOUD="https://cloud.".$domain;
$CLOUD_APP="/index.php/apps/webtransfer";
$CLOUD_SEND_ZIP="/zipDrop?subUrl=";
$CLOUD_SEND_FILE="/zipDrop?subUrl=";
#getZipFile

// ========================================
// CONSTANTES
define ('MAX_VALID_UPLOAD_TIME', 60);

define ('TOKEN_USE_LIMIT', "-2 hours");
define ('TOKEN_LOGIN_LIMIT', "-15 minutes");
define ('TOKEN_LOGOUT_LIMIT', "-8 hours");

if (!file_exists (VAR_CONFIG))
    mkdir (VAR_CONFIG, 0755);
if (!file_exists (FILE_CONFIG)) {
    file_put_contents (FILE_CONFIG, "<?php".NL.
                       "/* if error with DEFAULT_MODE, DEFAULT_PERIOD or DEFAULT_LANG then remove this file. */".NL.
                       "define ('DEFAULT_MODE', 'footer');".NL.
                       "define ('DEFAULT_PERIOD', 'month');".NL.
                       "define ('DEFAULT_LANG', 'fr');".NL.NL);
    define ('DEFAULT_MODE', 'footer');
    define ('DEFAULT_PERIOD', 'month');
    define ('DEFAULT_LANG', 'fr');
} else
    require (FILE_CONFIG);

// ========================================
// ERRORS
define ('E_BAD_ARCHIVE_NAME', 'Bad archive name format');
define ('E_CREATE_ZIP', "Impossible de cr&eacute;er l'archive.");
define ('E_OPEN_ZIP', "Impossible d'ouvrir l'archive.");

// ========================================
// MESSAGES
define ('M_BAD_KEY', "Mauvaise clef pour ");
define ('M_BAD_SENDER_NAME', 'Votre m&eacute;l est incorrect');
define ('M_BAD_TOKEN', "Vous n'utilisez pas le bon jeton (consultez votre messagerie).");
define ('M_TOO_LONG_BEFORE_LOGGED', "Jeton de connexion trop ancien.");
define ('M_TOO_LONG_LOGGED', "Temps de connexion d&eacute;pass&eacute;.");
define ('M_EMAIL_CONTENT', "Bonjour,<br/><br/>Ceci est un message automatique, car vous venez de cliquer sur une demande de consultation de vos pi&egrave;ces jointes.<br/><br/>!!! Si vous n'&ecirc;tes pas &agrave; l'origine de cette demande, ne cliquez sur aucun lien de ce message. !!!<br/><br/>Le lien de connexion suivant est valable 15 minutes.<br/><a href=\"___LINK___\">___LINK___</a><br/><br/>Vous pouvez signaler des abus aupr&egrave;s de Kaz en faisant suivre ce message qui contient les traces de son &eacute;metteur (___IP___, ___DATE___).<br/><br/>Bonne navigation.<br/>.");
define ('M_DOWNLOAD', "T&eacute;l&eacute;charger");
define ('M_UPDATE', "Prolonger");
define ('M_EMAIL_SUBJECT', "Lien de consultation des envois sur ".$domain.".");
define ('M_FILE', " fichier.");
define ('M_FILES', " fichiers.");
define ('M_FILES_NOT_FOUND', " fichiers sont expir&eacute;s.");
define ('M_FILES_RENAMED', " fichiers renomm&eacute;s.");
define ('M_FILE_NOT_FOUND', " fichier est expir&eacute;.");
define ('M_FILE_RENAMED', " fichier renomm&eacute;.");
define ('M_MEL', "votre m&eacute;l");
define ('M_NO_FILENAME', 'SansNom');
define ('M_NO_SENDER', 'kaz-');
define ('M_NEW_ATTACHEMENT_DIRNAME', "nouveau");
define ('M_OLD_ATTACHEMENT_DIRNAME', "ancien");
define ('M_SEND', "Connexion");
define ('M_LOGOUT', 'Deconnecter');
define ('M_REFRESH', 'Actualiser');
define ('M_CLOSE_PROFILE', "Votre profile est referm&eacute;.");
define ('M_LOGOUT_TOKEN', "Vous n'&ecirc;tes plus connect&eacute;.");
define ('M_TIMEOUT_TOKEN', "Votre session est expir&eacute;e.");
define ('M_SEND_TOKEN', "<br/><p>Vous allez recevoir un <b>lien d'acc&egrave;s temporaire</b> &agrave; vos donn&eacute;es.</p>");
define ('M_WELCOME', "<p>Informations concernant le compte : <b>___SENDER___</b>___ADMIN___<br/>(page actualis&eacute;e &agrave; ___DATE___)</p>");
define ('M_INCONSISTENT_DATES',
        " (dates incoh&eacute;antes avec ___FILENAME___ : ___DIRTIME___ != ___FILETIME___)");
define ('M_LOGIN_CONFIG', "<p>Pour personnaliser vos envoie de pi&egrave;ce jointes, indiquez votre m&eacute;l. Vous recevrez un lien de connexion s&eacute;curis&eacute;.</p>");

define ('M_SWITCH_WELCOME', "<p>Bienvenue dans la gare de triage de vos pi&egrave;ces jointes. Cette page permet d'orienter les pi&egrave;ces jointes que vous recevez vers vos espaces de stockage (vos \"clouds\") qui doivent poss&eacute;der l'extension webTransfert (c'est le cas des clouds chez Kaz). <a href=\"https://wiki.kaz.bzh/messagerie/cloud/\">voir la documentation en ligne</a>.<p>");
define ('M_SWITCH_NO_FILE', "<p>Actuellement, vous n'avez pas de pi&egrave;ces jointes &agrave; ranger. Cependant, vous pouvez pr&eacute;parer de futurs rangements en d&eacute;finissant la liste de vos clouds favoris.</p>");
define ('M_SWITCH_CUSTOM_URL', "C'est ici que vous ajoutez ou ordonnez la liste de vos clouds favoris pour y d&eacute;poser vos pi&egrave;ces jointes.");
define ('M_SWITCH_LOGIN', "<p>Personnaliser la liste de vos clouds favoris en indiquant votre m&eacute;l. Vous recevrez un lien de connexion s&eacute;curis&eacute;.</p>");
define ('M_SWITCH_LOGOUT', "Quand vous avez fini de personnaliser la liste de vos clouds favoris, pensez &agrave; vous d&eacute;connecter pour &eacute;viter que d'autres ne perturbe cette liste.");
define ('M_SWITCH_FORGET', "La liste des clouds favoris est concerv&eacute;e pendant 2 mois. Pour ne plus l'utiliser dans ce navigateur, il faut cliquer sur ");

define ('M_URL_ADDED', "<p>Le lien vient d'&ecirc;tre ajout&eacute;</p>");
define ('M_LIST_URL', "Choisisez un nuage pour stoker vos pi&egrave;ces jointes");
define ('M_NEW_URL', "Ou saisissez en un nouveau");


// ========================================
// PARAMETERS
define ('A_ACTION', 'a');			// action : T_LOGIN, T_LOGOUT, A_MODE(none|footer|attachment|both), A_RECORD+(on|off), A_PERIOD(minute|hour|day|week|month|quarter|semester), A_LANG(fr|en|br)
define ('A_OPEN_TOKEN', 'o');		// ask token
define ('A_TOKEN', 't');			// session token
define ('A_UPDATE', 'u');			// update perriod for file or archive
define ('A_MODE', 'm');				// get mode status
define ('A_RECORD', 'r');			// get track status
define ('A_PERIOD', 'p');			// get period status
define ('A_LANG', 'l');				// get lang status
define ('A_DELETE', 'd');			// delete file ou archive + (sender+token)
define ('A_SENDER', 's');			// session sender
define ('A_GET', 'g');				// get archive
define ('A_HASH', 'h');				// file to update or delete
define ('A_KEY', 'k');				// keyfile to update or delete
define ('A_NAME', "name");
define ('A_URL', "url");

// ========================================
// TOKENS
define ('T_BAD_PASW', 'bad_psw');
define ('T_CRYPTED', 'crypted');
define ('T_CRYPT_KEY', 'crypt_key');
define ('T_ENTRIES', 'entries');
define ('T_FILENAME', 'file_name');
define ('T_HASH', 'hash');
define ('T_NEW', 'new');
define ('T_SIGN', 'sign');
define ('T_NOT_FOUND', 'not_found');
define ('T_OLD', 'old');
define ('T_RENAME', 'rename');
define ('T_FORGETME', 'forget_me');
define ('T_LOGIN', 'login');
define ('T_LOGOUT', 'logout');
define ('T_PROFILE', 'profile');
define ('T_SENDER', 'sender');
define ('T_TIME', 'time');
define ('T_ID', 'id');
define ('T_WARNING_FILENAME', "-Avertissement.txt");
define ('T_ZIP_EXT', ".zip");
define ('T_ARCHIVE_TITLE', "archive_content");
define ('T_ARCHIVE_MIME', "text/kaz_email_archive");
define ('T_TOKEN', "token");
define ('T_CREATE', "create");
define ('T_LOGGED', "logged");
define ('T_ADD_URL', "add_url");
define ('T_DEL_URL', "del_url");
define ('T_UP_URL', "up_url");
define ('T_DOWN_URL', "down_url");
define ('T_URLS', "urls");

define ('L_ADD_URL', "&#9989;");
define ('L_DEL_URL', "&#x274C;");
define ('L_UP_URL', "&#x2191;");
define ('L_DOWN_URL', "&#x2193;");


/* Operations may take a long time.
 * Be sure PHP's safe mode is off.
 */
// @set_time_limit (0);
/* Remove errors. */
// @error_reporting (0);

require (JIRAFEAU_ROOT . 'lib/template/page.php');

// ========================================
/**
 * Supprime les autorisations de modification de profile de plus de 2 heures.
 */
function cleanToken () {
    if (!file_exists (VAR_TOKENS))
        mkdir (VAR_TOKENS, 0755);
    $d = dir (VAR_TOKENS);
    $oldest = strtotime (TOKEN_USE_LIMIT);
    foreach (glob (VAR_TOKENS."*") as $file) {
        if (file_exists ($file) && filemtime ($file) <= $oldest)
            unlink ($file);
    }
}

/**
 * Supprime une autorisation spécifique (déconnexion)
 */
function rmToken ($sender) {
    if (!$sender)
        return;
    if (file_exists (VAR_TOKENS.$sender))
        unlink (VAR_TOKENS.$sender);
}

function setToken ($sender) {
    if (!$sender)
        return;
    $token = md5 (rand ());
    if (file_put_contents (VAR_TOKENS.$sender, T_CREATE.": ".time ().NL.T_TOKEN.": ".$token.NL))
        return $token;
    return false;
}

function setLoggedToken ($sender, $token) {
    if (!$sender || !$token)
        return;
    file_put_contents (VAR_TOKENS.$sender, T_CREATE.": ".time ().NL.T_TOKEN.": ".$token.NL.T_LOGGED.": ok".NL);
}

/**
 * Récupère une variable spécifique d'un token.
 *
 * @param string $sender Nom du token.
 * @param string $varName Nom de la variable.
 * @return string|false La valeur ou false.
 */
function getTokenVar ($sender, $varName) {
    if (!$sender)
        return;
    if (!file_exists (VAR_TOKENS.$sender))
        return false;
    $content = file_get_contents (VAR_TOKENS.$sender);
    if (preg_match ("/\b".$varName.":\s*([^\s]+)\n/", $content, $matches))
        return $matches [1];
    return false;
}

function getToken ($sender) {
    return getTokenVar ($sender, T_TOKEN);
}
function getCreateToken ($sender) {
    return getTokenVar ($sender, T_CREATE);
}
function getLoggedToken ($sender) {
    return getTokenVar ($sender, T_LOGGED);
}
function getTimeToken ($sender) {
    if (!$sender || !file_exists (VAR_TOKENS.$sender))
        return false;
    return filemtime (VAR_TOKENS.$sender);
}

// ========================================
/**
 * Outils en cas de fraude
 */
function setSenderFake ($error, $sender, $owner, $dirLink, $fileLink) {
    global $doLogout;

    if (!file_exists (VAR_FAKE))
        mkdir (VAR_FAKE, 0755);
    $dirTime = $fileTime = $fileName = $fileType = $ip = '';
    if (count ($dirLink) != 0) {
        $dirTime = $dirLink ['upload_date'].date (" Y-m-d H:i:s", $dirLink ['upload_date']);
        $ip = $dirLink ['ip'];
    }
    if (!$sender)
        return;
    if (count ($fileLink) != 0) {
        $fileTime = $fileLink ['upload_date'].date (" Y-m-d H:i:s", $fileLink ['upload_date']);
        $fileName = $link ['file_name'];
        $fileType = $link ['mime_type'];
    }
    $content =
             "time    : ".time ().NL.
             "date    : ".date ("Y-m-d H:i:s").NL.
             "error   : ".$error.NL.
             "sender  : <".$sender.">".NL.
             "owner   : <".$owner.">".NL.
             "dirLink : <".$dirLink.">".NL.
             "dirTime : ".$dirTime.NL.
             "dirIp   : ".$ip.NL.
             "fileTime: ".$fileTime.NL.
             "fileType: <".$fileType.">".NL.
             "fileName: <".$fileName.">".NL;
    
    $log = $ip.$sender;
    if ($log)
        file_put_contents (VAR_FAKE.$log, $content);

    // $doLogout = true;
    // rmToken ($sender);
}
function getSenderFake ($sender) {
    return false;
    // return $sender && file_exists (VAR_FAKE.$sender);
}

// ========================================
/**
 * Envoie un e-mail via PHPMailer.
 *
 * @param string $receiver Destinataire.
 * @param string $receiver_name Nom du destinataire.
 * @param string $subject Sujet de l'e-mail.
 * @param string $body_string Corps de l'e-mail.
 * @return bool Succes de l'envoi.
 */
function sendEMail ($receiver, $receiver_name,  $subject, $body_string){
    try {
        // SERVER SETTINGS
        $mail = new PHPMailer (true);
        $mail->isSMTP ();
        $mail->Host        = 'smtp';
        $mail->SMTPAuth    = false;
        $mail->SMTPAutoTLS = false;
        $mail->SMTPSecure  = "none";
        $mail->Port        = 25;
        $mail->charSet     = "UTF-8";
        $mail->ContentType = 'text/html';

        global $domain;
        //Recipients (change this for every project)
        $mail->setFrom ('no-reply@'.$domain, '');
        $mail->addAddress ($receiver, $receiver_name);

        //Content
        $mail->isHTML (true);
        $mail->Subject = $subject;
        $mail->Body    = $body_string;

        //send the message, check for errors
        if (!$mail->send ()) {
            //echo 'Mailer Error: ' . $mail->ErrorInfo;
            return 0;
        } else {
            //echo 'Message sent!';
            return 1;
        }
    } catch (Exception $e) {
        return 0;
    }
}

// ========================================
// setup
$message = '';
$sender = '';
$senderError = false;
if (isset ($_REQUEST [A_SENDER]) && !empty ($_REQUEST [A_SENDER])) {
    if (!preg_match ("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i", $_REQUEST [A_SENDER]))
        $senderError = true;
    else {
        cleanToken ();
        $sender = filter_var ($_REQUEST [A_SENDER], FILTER_VALIDATE_EMAIL);
    }
}

$token = '';
if (isset ($_REQUEST [A_TOKEN]) && !empty ($_REQUEST [A_TOKEN])) {
    if (!preg_match ("/^([0-9a-zA-Z_-]+)$/", $_REQUEST [A_TOKEN]))
        return false;
    $token = $_REQUEST [A_TOKEN];
}

$refToken = getToken ($sender);
$urlBase = $_SERVER ['HTTP_X_FORWARDED_PROTO']."://".$_SERVER ['HTTP_HOST'];
if (isset ($_REQUEST [A_ACTION]) && $_REQUEST [A_ACTION] == T_LOGIN && $sender) {
    displayHeadPage ("Erreur");
    if (getSenderFake ($sender))
        echo "Ce compte ne peut plus se connecter. Veuillez contacter les administrateurs.";
    else {
        $token = setToken ($sender);
        // XXX test token
        $url = $urlBase.$_SERVER ['SCRIPT_NAME']."?".A_SENDER."=".$sender."&".A_TOKEN."=".$token;
        if (isset ($_REQUEST [A_GET]))
            $url .= "&".A_GET."=".$_REQUEST [A_GET];
        if (isset ($_REQUEST [A_HASH]))
            $url .= "&".A_HASH."=".$_REQUEST [A_HASH];
        $result = sendEMail ($sender, "",  M_EMAIL_SUBJECT,
                             str_replace (["___LINK___", "___IP___", "___DATE___"],
                                          [$url, $_SERVER ['HTTP_X_REAL_IP'], date ("Y-m-d H:i:s")], M_EMAIL_CONTENT));
        if ($result)
            echo M_SEND_TOKEN;
        else
            echo
                "Erreur dans l'envoi. V&eacute;ritiez votre m&eacute;l.";
    }
    echo "<br/><br/><br/>";
    displayFootPage ();
    exit;
}

function getHiddenToken () {
    global $sender, $token;
 return
        '    <input type="hidden" name="'.A_SENDER.'" value="'.$sender.'">'.NL.
        '    <input type="hidden" name="'.A_TOKEN.'" value="'.$token.'">'.NL;
}

function getHiddenLink () {
    $result = '';
    if (isset ($_REQUEST [A_GET]))
        $result .=
                '    <input type="hidden" name="'.A_GET.'" value="'.htmlspecialchars ($_REQUEST [A_GET]).'" />'.NL;
    if (isset ($_REQUEST [A_HASH]))
        $result .=
                '    <input type="hidden" name="'.A_HASH.'" value="'.htmlspecialchars ($_REQUEST [A_HASH]).'" />'.NL;
    return $result;
    if (isset ($_REQUEST [A_KEY]))
        $result .=
                '    <input type="hidden" name="'.A_KEY.'" value="'.htmlspecialchars ($_REQUEST [A_KEY]).'" />'.NL;
}

