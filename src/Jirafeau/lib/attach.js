const toggleTheme = document.getElementById('toggle-theme');
const body = document.body;

toggleTheme.addEventListener('change', () => {
    if (toggleTheme.checked) {
        body.classList.remove('light-theme');
        body.classList.add('dark-theme');
    } else {
        body.classList.remove('dark-theme');
        body.classList.add('light-theme');
    }

    document.cookie = "theme=" + (toggleTheme.checked ? "dark-theme" : "light-theme") + "; path=/; max-age=" + (60 * 60 * 24 * 30);
});

function forwardQuerry (form, query) {
    try {
	var urlBase = new URL (form ["url"].value);
	// XXX test host non vide
	url = urlBase+query;
	var link = document.createElement ('a');
	link.href = url;
	document.body.appendChild (link);
	link.click ();
    } catch (_) {
	alert ("url incorrecte");
	return false;
    }
}

function actionUrl (button, check, name, value) {
    if (check && ! confirm("Êtes-vous sûr de vouloir supprimer ?"))
	return;
    form = button.parentElement;
    input = document.createElement ('input');
    input.setAttribute ('name', name);
    input.setAttribute ('value', value);
    input.setAttribute ('type', 'hidden');
    form.appendChild(input);
    form.submit ();
}
