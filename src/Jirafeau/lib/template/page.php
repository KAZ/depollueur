<?php
 function displayHeadPage ($title) {
 ?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title><?php echo $title; ?></title>
    <link rel="stylesheet" href="media/kaz/attach.css">
  </head>
  <body class="<?php echo isset ($_COOKIE['theme']) ? htmlspecialchars ($_COOKIE['theme']) : 'light-theme'; ?>">
    <div class="theme-toggle">
      <input type="checkbox" id="toggle-theme" <?php echo (isset($_COOKIE['theme']) && $_COOKIE['theme'] === 'dark-theme') ? 'checked' : ''; ?>/>
      <label for="toggle-theme" class="toggle-label">
        <span class="toggle-dark">&#x1F319;</span>
        <span class="toggle-light">&#x1F506;</span>
      </label>
    </div>
    <div class="container">
      <div class="main-box">
	<div class="blocks">
	  <?php
	   }

	   function displayFootPage () { ?>
	</div>
      </div>
    </div>
    <script src="lib/attach.js"></script>
    <div id="kaz"/>
  </body>
</html>
<?php
 }

 function displayRefresh () { ?>
<form action="<?php echo $_SERVER ['SCRIPT_NAME']; ?>" method="POST">
  <?php echo getHiddenToken (); ?><?php echo getHiddenLink (); ?>
  <input type="submit" value="<?php echo M_REFRESH; ?>">
</form>
<?php
 }

 function displayLogin ($msg) { ?>
<div class="block">
  <div class="block-info"><p><?php echo $msg; ?></p></div>

  
  <div class="table">
    <form class="tr" action="<?php echo $_SERVER ['SCRIPT_NAME']; ?>" method="POST">
      <input type="email" name="<?php echo A_SENDER; ?>" size="40" value="<?php echo jirafeau_escape ($_REQUEST [A_SENDER]);?>" placeholder="Votre email" required="required"/>
      <?php echo getHiddenLink (); ?>
      <input type="hidden" name="<?php echo A_ACTION; ?>" />
      <input type="submit" value="<?php echo M_SEND; ?>" />
    </form>
  </div>
</div>
<?php
 }

 function displayLogout ($msg) { ?>
<div class="block">
  <div class="block-info"><p><?php echo $msg; ?></p></div>
  <div class="table">
    <form class="tr" action="<?php echo $_SERVER ['SCRIPT_NAME']; ?>" method="POST">
      <input type="hidden" name="<?php echo A_ACTION; ?>" value="<?php echo T_LOGOUT; ?>">
      <?php echo getHiddenToken (); ?>
      <?php echo getHiddenLink (); ?>
      <button type="submit">D&eacute;connexion</button>
    </form>
  </div>
</div>
<?php
 }

 function displayProfileName ($msg) { ?>
<div class="block">
  <div class="block-info"><p><?php echo $msg; ?></p></div>
  <div class="table">
    <form class="tr" action="<?php echo $_SERVER ['SCRIPT_NAME']; ?>" method="POST">
      <input type="hidden" name="<?php echo A_ACTION; ?>" value="<?php echo T_FORGETME; ?>">
      <?php echo getHiddenLink (); ?>
      <button type="submit">Oubliez-moi</button>
    </form>
  </div>
</div>
<?php
 }

 function displayListUrl ($query, $listUrl) {
 global $DEFAULT_CLOUD;
 ?>
<div class="block">
  <div class="block-info"><p><?php echo M_LIST_URL; ?></p></div>
  <div class="management-table cloud-list">
    <div class="tr">
    </div><?php if (isset ($listUrl['urls'])) foreach ($listUrl['urls'] as $name => $url) { ?>
    <div class="tr">
      <span class="td"><a href="<?php echo $url.$query; ?>" class="cloud-item"><?php echo htmlspecialchars ($name);?></a></span><span class="td"><span class="comment"><?php echo htmlspecialchars ($url); ?></span></span>
    </div><?php } ?>
    <form class="tr" onsubmit="return forwardQuerry (this,<?php echo "'".$query."'"; ?>);">
      <span class="td comment"><?php echo M_NEW_URL; ?></span><span class="td"><input name="url" type="text" value="<?php echo $DEFAULT_CLOUD; ?>" placeholder="https://..."/></span>
    </form>
  </div>
</div>
<?php
 }

 function displayFormProfile ($listUrl) {
 global $name, $url;
 ?>
<div class="block">
  <div class="block-info"><p><?php echo M_SWITCH_CUSTOM_URL; ?></p></div>
  <div class="table">
    <form class="tr" action="<?php echo $_SERVER ['SCRIPT_NAME']; ?>" method="POST">
      <span class="td"></span>
      <span class="td"><input name="<?php echo A_NAME;?>" type="text" value="<?php echo $name;?>" placeholder="Mon nuage"/></span>
      <span class="td"><input name="<?php echo A_URL;?>" type="text" value="<?php echo $url;?>" placeholder="https://..."/></span>
      <?php echo getHiddenToken (); ?><?php echo getHiddenLink (); ?>
      <span class="td"><button class="btn btn-new" onclick="actionUrl (this, false, '<?php echo A_ACTION; ?>', '<?php echo T_ADD_URL; ?>')" ><?php echo L_ADD_URL; ?></button></span>
    </form><?php if (isset ($listUrl['urls'])) foreach ($listUrl['urls'] as $name2 => $url2) { ?>
    <form class="tr" action="<?php echo $_SERVER ['SCRIPT_NAME']; ?>" method="POST">
      <span class="td">
	<button class="btn btn-up" onclick="actionUrl (this, false, '<?php echo A_ACTION; ?>', '<?php echo T_UP_URL; ?>')" ><?php echo L_UP_URL; ?></button>
	<button class="btn btn-down" onclick="actionUrl (this, false, '<?php echo A_ACTION; ?>', '<?php echo T_DOWN_URL; ?>')" ><?php echo L_DOWN_URL; ?></button>
      </span>
      <span class="td"><?php echo htmlspecialchars ($name2);?></span>
      <span class="td"><?php echo htmlspecialchars ($url2);?></span>
      <input name="<?php echo A_NAME;?>" type="hidden" value="<?php echo $name2;?>"/>
      <?php echo getHiddenToken (); ?><?php echo getHiddenLink (); ?><span class="td">
	<button class="btn btn-delete" onclick="actionUrl (this, true, '<?php echo A_ACTION; ?>', '<?php echo T_DEL_URL; ?>')" ><?php echo L_DEL_URL; ?></button>
      </span>
    </form><?php } ?>
  </div>
</div>
<?php
 }
