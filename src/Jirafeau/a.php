<?php
/*
 *  Kaz addon (see https://git.kaz.bzh/KAZ/depollueur for information)
 *  create un archive for a set of file or update file deadline

 a.php?r=email => track
 a.php?p=email => period
 a.php?u=month&h=HHHHHHHH => update deadline
 a.php?g=l~k => zip
 a.php?time=month&key=password + POST file => upload
 a.php?s=mel@domain.org => form
 a.php?s=mel@domain.org&t=password + [action] => manage account
 action: a=login a=logout a=r[on|off] a=p[minute|hour|day|week|month|quarter|semester]
*/

require ("lib/attach-setup.php");

$modeText = ['none' => "sans", 'footer' => "pied de page", 'attachment' => "pi&egrave;ce jointe", 'both' => "les deux"];
$trackText = ['on' => "oui", 'off' => "non"];
$periodText = ['minute' => "minute", 'hour' => "heure", 'day' => "jour", 'week' => "semaine", 'month' => "mois", 'quarter' => "trimestre", "semester" => "semestre"];
$periodButton = ['hour' => ["&#128336;", ">1 heure"],			// 1F550
                 'day' => ["&#128337;", ">1 jour"],				// 1F551
                 'week' => ["&#128338;", "> 1 semaine"],		// 1F552
                 'month' => ["&#128339;", "> 1 mois"],			// 1F553
                 'quarter' => ["&#128340;", "> 1 trimestre"],	// 1F554
                 'semester' => ["&#128341;", "> 1 semestre"]	// 1F555
];
$langText = ['fr' => "Francais", 'br' => "Breton", 'en' => "english"];
$doLogout = '';

// ========================================
if (isset ($_REQUEST [A_MODE]) && !empty ($_REQUEST [A_MODE])) {
    if (!preg_match ("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i", $_REQUEST [A_MODE]))
        $content = DEFAULT_MODE.NL;
    else
        $content = getSenderMode ($_REQUEST [A_MODE]).NL;
    header ('HTTP/1.0 200 OK');
    header ('Content-Length: ' . strlen ($content));
    header ('Content-Type: text/plain');
    echo $content;
    exit;
}

// ========================================
if (isset ($_REQUEST [A_RECORD]) && !empty ($_REQUEST [A_RECORD])) {
    if (!preg_match ("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i", $_REQUEST [A_RECORD]))
        $content = false.NL;
    else
        $content = isSenderTrack ($_REQUEST [A_RECORD]).NL;
    header ('HTTP/1.0 200 OK');
    header ('Content-Length: ' . strlen ($content));
    header ('Content-Type: text/plain');
    echo $content;
    exit;
}

// ========================================
if (isset ($_REQUEST [A_PERIOD]) && !empty ($_REQUEST [A_PERIOD])) {
    if (!preg_match ("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i", $_REQUEST [A_PERIOD]))
        $content = DEFAULT_PERIOD.NL;
    else
        $content = getSenderPeriod ($_REQUEST [A_PERIOD]).NL;
    header ('HTTP/1.0 200 OK');
    header ('Content-Length: ' . strlen ($content));
    header ('Content-Type: text/plain');
    echo $content;
    exit;
}

// ========================================
if (isset ($_REQUEST [A_LANG]) && !empty ($_REQUEST [A_LANG])) {
    if (!preg_match ("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/i", $_REQUEST [A_LANG]))
        $content = DEFAULT_LANG.NL;
    else
        $content = getSenderLang ($_REQUEST [A_LANG]).NL;
    header ('HTTP/1.0 200 OK');
    header ('Content-Length: ' . strlen ($content));
    header ('Content-Type: text/plain');
    echo $content;
    exit;
}

// ========================================
$doUpdate = false;
if (isset ($_REQUEST [A_UPDATE]) && !empty ($_REQUEST [A_UPDATE])) {
    $doUpdate = true;
}

$doDownload = false;
if (isset ($_REQUEST [A_GET]) && !empty ($_REQUEST [A_GET])) {
    $doDownload = true;
}

// ========================================
function returnError ($msg) {
    displayHeadPage ("Gestion des pièces jointes");
    echo '<div class="message-error">' . $msg . '</div>';
    displayFootPage ();
    exit;
}

if (isset ($_FILES ['file'])) {
    // XXX t ("NoUpload")
    returnError ("Can't upload file");
}

// ========================================
function setSenderMode ($sender, $mode) {
    if (!$sender)
        return;
    if (!file_exists (VAR_MODE))
        mkdir (VAR_MODE, 0755);
    if (empty ($mode) || DEFAULT_MODE == $mode) {
        rmSenderMode ($sender);
    } else
        file_put_contents (VAR_MODE.$sender, $mode.NL);
}
function rmSenderMode ($sender) {
    if (!$sender)
        return;
    if (file_exists (VAR_MODE.$sender))
        unlink (VAR_MODE.$sender);
}
function getSenderMode ($sender) {
    if ($sender && file_exists (VAR_MODE.$sender))
        return trim (file (VAR_MODE.$sender)[0]);
    return DEFAULT_MODE;
}

// ========================================
function setSenderTrack ($sender) {
    if (!$sender)
        return;
    if (!file_exists (VAR_TRACKS))
        mkdir (VAR_TRACKS, 0755);
    touch (VAR_TRACKS.$sender);
}
function rmSenderTrack ($sender) {
    if (!$sender)
        return;
    if (file_exists (VAR_TRACKS.$sender))
        unlink (VAR_TRACKS.$sender);
}
function isSenderTrack ($sender) {
    return $sender && file_exists (VAR_TRACKS.$sender);
}

// ========================================
function setSenderPeriod ($sender, $period) {
    if (!$sender)
        return;
    if (!file_exists (VAR_PERIOD))
        mkdir (VAR_PERIOD, 0755);
    if (empty ($period) || DEFAULT_PERIOD == $period) {
        rmSenderPeriod ($sender);
    } else
        file_put_contents (VAR_PERIOD.$sender, $period.NL);
}
function rmSenderPeriod ($sender) {
    if (!$sender)
        return;
    if (file_exists (VAR_PERIOD.$sender))
        unlink (VAR_PERIOD.$sender);
}
function getSenderPeriod ($sender) {
    if ($sender && file_exists (VAR_PERIOD.$sender))
        return trim (file (VAR_PERIOD.$sender)[0]);
    return DEFAULT_PERIOD;
}
function period2seconds ($periodName) {
    if (!$periodName)
        return JIRAFEAU_MONTH;
    switch ($periodName) {
    case 'minute':
        return JIRAFEAU_MINUTE;
        break;
    case 'hour':
        return JIRAFEAU_HOUR;
        break;
    case 'day':
        return JIRAFEAU_DAY;
        break;
    case 'week':
        return JIRAFEAU_WEEK;
        break;
    case 'month':
        return JIRAFEAU_MONTH;
        break;
    case 'quarter':
        return JIRAFEAU_QUARTER;
        break;
    case 'semester':
        return JIRAFEAU_SEMESTER;
        break;
    case 'year':
        return JIRAFEAU_YEAR;
        break;
    default:
        returnError (t ('ERR_OCC') . ' (periodName)');
    }
}

// ========================================
function setSenderLang ($sender, $lang) {
    if (!$sender)
        return;
    if (!file_exists (VAR_LANG))
        mkdir (VAR_LANG, 0755);
    if (empty ($lang) || DEFAULT_LANG == $lang) {
        rmSenderLang ($sender);
    } else
        file_put_contents (VAR_LANG.$sender, $lang.NL);
}
function rmSenderLang ($sender) {
    if (!$sender)
        return;
    if (file_exists (VAR_LANG.$sender))
        unlink (VAR_LANG.$sender);
}
function getSenderLang ($sender) {
    if ($sender && file_exists (VAR_LANG.$sender))
        return trim (file (VAR_LANG.$sender)[0]);
    return DEFAULT_LANG;
}

// ========================================
function isKazArchive ($link) {
    return
        @preg_match ("/".T_ARCHIVE_TITLE."/", jirafeau_escape ($link ['file_name'])) &&
        jirafeau_escape ($link ['mime_type']) == T_ARCHIVE_MIME;
}

// ========================================
function readArchiveFromLink ($link) {
    $p = s2p ($link ['hash']);
    $lines = file (VAR_FILES . $p . $link ['hash']);
    $archive = readArchiveFromLines ($lines);
    return $archive;
}
function readArchiveFromLines ($lines) {
    $archive = [];
    $error = false;
    foreach ($lines as $line) {
        switch (true) {
        case preg_match ("/^\s*id:\s*(\d++)\s*$/", $line, $matches):
            $archive [T_ID] = $matches [1];
            break;
        case preg_match ("/^\s*time:\s*(\d{4}([:-]\d{2}){5})\s*$/i", $line, $matches):
            $archive [T_TIME] = $matches [1];
            break;
            // XXX
            //case preg_match ("/^\s*sender:\s*(([a-z0-9_+-]+)(\.[a-z0-9_+-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6})\s*$/i", $line, $matches):
            case preg_match ("/^\s*sender:\s*(([a-z0-9_=+-]+)(\.[a-z0-9_=+-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6})\s*$/i", $line, $matches):
            $archive [T_SENDER] = $matches [1];
            break;
            // XXX issue ")"
        case preg_match ("/^\s*new:\s*([0-9a-zA-Z_-]+)\s+([0-9a-zA-Z_-]+)\)?\s*$/", $line, $matches):
            $archive [T_NEW][] = [$matches [1], $matches [2]];
            break;
        case preg_match ("/^\s*old:\s*([0-9a-zA-Z_-]+)\s+([0-9a-zA-Z_-]+)\)?\s*$/", $line, $matches):
            $archive [T_OLD][] = [$matches [1], $matches [2]];
            break;
        case preg_match ("/^\s*sign:\s*([0-9a-zA-Z_-]+)\s*$/", $line, $matches):
            $archive [T_SIGN] = $matches [1];
            break;
        default:
            $error = true;
            break;
        }
    }
    global $message, $admin;
    if ($error && $admin)
        $message .= "readArchiveFromLines <pre>".print_r ($lines, true)."</pre>";
    return $error ? [] : $archive;
}

// ========================================
function getFileName ($hash) {
    $p = s2p ($hash);
    return VAR_FILES.$p.$hash;
}
function getTimeFile ($hash) {
    $f = getFileName ($hash);
    return file_exists ($f) ? filemtime ($f) : 0;
}
function valideTime ($t1, $t2) {
    global $message;
    return abs ($t1 - $t2) < MAX_VALID_UPLOAD_TIME;
}

// ========================================
/** Update link 
 * @param $link the link's name (hash)
 * @param $update_period the period (i.e in : "month")
 */
function updateLink ($link_name, $link, $maxLimit) {
    $time_max = $link ['time'];
    if ($time_max < 0 || $maxLimit < $time_max)
        return $time_max;
    $time_more = $maxLimit + JIRAFEAU_MINUTE;
    $link ['time'] = $time_more;
    $link_tmp_name =  VAR_LINKS . $link ['hash'] . rand (0, 10000) . '.tmp';
    $handle = fopen ($link_tmp_name, 'w');
    fwrite ($handle,
            $link ['file_name'] .NL. $link ['mime_type'] .NL. $link ['file_size'] .NL. $link ['key'] .NL. $link ['time'] .NL.
            $link ['hash'] .NL. $link ['onetime'] .' '.JIRAFEAU_MONTH . ' '. JIRAFEAU_DAY .NL. $link ['upload_date'] .NL.
            $link ['ip'] .NL. $link ['link_code'] .NL. $link ['crypted']);
    fclose ($handle);
    $link_file = VAR_LINKS . s2p ("$link_name") . $link_name;
    rename ($link_tmp_name, $link_file);
    return $time_more;
}

// ========================================
function setAdmin ($sender) {
    if (!$sender)
        return;
    if (!file_exists (VAR_ADMIN))
        mkdir (VAR_ADMIN, 0755);
    touch (VAR_ADMIN.$sender);
}
function rmAdmin ($sender) {
    if (!$sender)
        return;
    if (file_exists (VAR_ADMIN.$sender))
        unlink (VAR_ADMIN.$sender);
}
function isAdmin ($sender) {
    return $sender && file_exists (VAR_ADMIN.$sender);
}

// ========================================
function deleteAction ($linkName) {
    global $sender, $token, $message, $doLogout;

    $link = jirafeau_get_link ($linkName);
    //$message .= "ln: ".$linkName." l: "."<pre>".print_r ($link, 1)."</pre> mt: ".getTimeFile ($link ['hash'])."</br>";
    if (!count ($link))
        return;
    if (isKazArchive ($link)) {
        $dirName = $linkName;
        $dirLink = $link;
        $dirTime = $dirLink ['upload_date'];
        $archiveInfo = readArchiveFromLink ($dirLink);
        if (! count ($archiveInfo))
            return;
        if ($sender != $archiveInfo [T_SENDER]) {
            setSenderFake ("rmdir: not owner", $sender, $archiveInfo [T_SENDER], $dirLink, null);
            $message .= "Tentative de supprimer un envoi dont vous n'&ecirc;tes pas le propri&eacute;taire";
            return;
        }
        $fileToDelete = false;
        if ($archiveInfo [T_NEW])
            foreach ($archiveInfo [T_NEW] as [$fileName, $cryptKey]) {
                $fileLink = jirafeau_get_link ($fileName);
                if (! count ($fileLink))
                    continue;
                $fileTime = $fileLink ['upload_date'];
                if (! valideTime ($dirTime, $fileTime)) {
                    setSenderFake ("rmdir: newfile not same time", $sender, null, $dirLink, $fileLink);
                    $message .= "Cet envoi a &eacute;t&eacute; forg&eacute;e".
                             str_replace (["___FILENAME___", "___DIRTIME___", "___FILETIME___"],
                                          [$fileLink ['file_name'], $dirTime , $fileTime], M_INCONSISTENT_DATES);
                    return;
                }
                $fileToDelete = true;
            }
        $message .= "l'envoi ".$archiveInfo [T_TIME]." est supprim&eacute;";
        if ($fileToDelete)
            $message .= " avec<ul>";
        if ($archiveInfo [T_NEW])
            foreach ($archiveInfo [T_NEW] as [$fileName, $cryptKey]) {
                $fileLink = jirafeau_get_link ($fileName);
                if (! count ($fileLink))
                    continue;
                $message .= "<li>".jirafeau_escape ($fileLink ['file_name'])."</li>";
                jirafeau_delete_link ($fileName);
            }
        jirafeau_delete_link ($dirName);
        $message .= $fileToDelete ? "</ul>" : ".";
        return;
    }
    $fileName = $linkName;
    $fileLink = $link;
    $fileTime = $fileLink ['upload_date'];
    $stack = array (VAR_LINKS);
    while (($d = array_shift ($stack)) && $d != null) {
        if (!file_exists ($d))
            continue;
        $dir = scandir ($d);
        foreach ($dir as $dirName) {
            if (strcmp ($dirName, '.') == 0 || strcmp ($dirName, '..') == 0 ||
                preg_match ('/\.tmp/i', "$dirName")) {
                continue;
            }
            if (is_dir ($d . $dirName)) {
                $stack [] = $d . $dirName . '/';
                continue;
            }
            $dirLink = jirafeau_get_link ($dirName);
            //$dirTime = getTimeFile ($dirLink ['hash']);
            $dirTime = $dirLink ['upload_date'];
            if (!count ($dirLink))
                continue;
            if (!isKazArchive ($dirLink))
                continue;
            $archiveInfo = readArchiveFromLink ($dirLink);
            if (! count ($archiveInfo))
                return;
            if ($archiveInfo [T_NEW])
                foreach ($archiveInfo [T_NEW] as [$newName, $cryptKey]) {
                    if ($fileName != $newName)
                        continue;
                    if ($sender == $archiveInfo [T_SENDER]) {
                        if (valideTime ($dirTime, $fileTime)) {
                            jirafeau_delete_link ($fileName);
                            $message .= jirafeau_escape ($fileLink ['file_name'])." est supprim&eacute;";
                            // check empty dir
                            $empty = true;
                            foreach ([T_OLD, T_NEW] as $cat)
                                if ($empty && isset ($archiveInfo [$cat]))
                                    foreach ($archiveInfo [$cat] as [$l, $c])
                                        if (count (jirafeau_get_link ($l))) {
                                            $empty = false;
                                            break;
                                        }
                            if ($empty) {
                                $message .= " ainsi que l'envoie ".$archiveInfo [T_TIME]." qui est vide.";
                                jirafeau_delete_link ($dirName);
                            } else
                                $message .= ".";
                            break;
                        }
                        setSenderFake ("rm: dir not same time", $sender, null, $dirLink, $fileLink);
                        $message .= "Cet envoi a &eacute;t&eacute; forg&eacute;e. ".
                                 str_replace (["___FILENAME___", "___DIRTIME___", "___FILETIME___"],
                                              [$fileLink ['file_name'], $dirTime , $fileTime], M_INCONSISTENT_DATES);
                        break;
                    }
                    if (valideTime ($dirTime, $fileTime)) {
                        setSenderFake ("rm: not owner", $sender, $archiveInfo [T_SENDER], $dirLink, $fileLink);
                        $message .= "Tentative de supprimer un envoi dont vous n'&ecirc;tes pas le propri&eacute;taire.".
                                 str_replace (["___FILENAME___", "___DIRTIME___", "___FILETIME___"],
                                              [$fileLink ['file_name'], $dirTime , $fileTime], M_INCONSISTENT_DATES);
                        break;
                    }
                    setSenderFake ("rm: find not owner", $archiveInfo [T_SENDER], $sender, $dirLink, $fileLink);
                    $message .= "Quelqu'un av&eacute;tait revandiqu&eacute; cet envoi. (".$sender." != ".$archiveInfo [T_SENDER].")";
                    break;
                }
        }
    }
}

// ========================================
// main
// ========================================
if ($doUpdate) {
    $maxTime = time ()+period2seconds ($_REQUEST [A_UPDATE]);
    // XXX issue ")"
    if (!preg_match ('/([0-9a-zA-Z_-]+)\)?$/', $_REQUEST [A_HASH], $matches))
        returnError (t ('FILE_404'));
    $linkName = $matches [1];
    $link = jirafeau_get_link ($linkName);
    if (count ($link) == 0)
        returnError (t ('FILE_404'));
    $time = updateLink ($linkName, $link, $maxTime);
    $content = '' . $time . NL;

    if (isKazArchive ($link)) {
        $archiveInfo = readArchiveFromLink ($l);
        if (count ($archiveInfo)) {
            foreach ([T_OLD, T_NEW] as $cat)
                if (isset ($archiveInfo [$cat]))
                    foreach ($archiveInfo [$cat] as [$linkName, $cryptKey])
                        updateLink ($linkName, jirafeau_get_link ($linkName), $maxTime);
        }
    }
    header ('HTTP/1.0 200 OK');
    header ('Content-Length: ' . strlen ($content));
    header ('Content-Type: text/plain');
    echo $content;
    exit;
}

// ========================================
if ($doDownload) {
    // check archive exist
    $couple = explode ("~", $_REQUEST [A_GET], 2);
    if (count ($couple) == 0)
        returnError (E_BAD_ARCHIVE_NAME);
    $linkName = $couple [0];
    if (!$linkName || !preg_match ('/[0-9a-zA-Z_-]+$/', $linkName))
        returnError (E_BAD_ARCHIVE_NAME);
    $cryptKey = count ($couple) == 2 ? $couple [1] : "";
    $link = jirafeau_get_link ($linkName);
    if (count ($link) == 0)
        returnError (t ('FILE_404'));
    $key = $link ['key'];
    if ($key && (empty ($cryptKey) || $key != $cryptKey))
        returnError (t ('BAD_PSW'));

    $archiveInfo = readArchiveFromLink ($link);

    // check entries
    $archiveContent = [];
    $modif = false;
    $singleName = [];
    foreach ([T_OLD, T_NEW] as $cat)
        if (isset ($archiveInfo [$cat]))
            foreach ($archiveInfo [$cat] as [$linkName, $cryptKey]) {
                $link = jirafeau_get_link ($linkName);
                if (count ($link) == 0) {
                    if (isset ($archiveContent [T_NOT_FOUND]))
                        ++$archiveContent [T_NOT_FOUND];
                    else
                        $archiveContent [T_NOT_FOUND] = 1;
                    $modif = true;
                    continue;
                }
                $key = $link ['key'];
                if ($key && (empty ($cryptKey) || $key != $cryptKey)) {
                    if (isset ($archiveContent [T_BAD_PASW]))
                        ++$archiveContent [T_BAD_PASW];
                    else
                        $archiveContent [T_BAD_PASW] = 1;
                    $modif = true;
                    continue;
                }
                $dstName = ($link ['file_name'] ? $link ['file_name'] : M_NO_FILENAME);
                if (in_array ($dstName, $singleName)) {
                    $dstFilename = $dstName;
                    $dstExtension = "";
                    $dstExtensionPos = strrpos ($dstName, '.');
                    if ($dstExtensionPos) {
                        $dstFilename = substr ($dstName, 0, $dstExtensionPos);
                        $dstExtension = substr ($dstName, $dstExtensionPos);
                    }
                    for ($i = 1; $i < 10000; ++$i) {
                        $dstName = sprintf ("%s-%02d%s", $dstFilename, $i, $dstExtension);
                        if (!in_array ($dstName, $singleName)) {
                            if (isset ($archiveContent [T_RENAME]))
                                ++$archiveContent [T_RENAME];
                            else
                                $archiveContent [T_RENAME] = 1;    
                            $modif = true;
                            break;
                        }
                    }
                }
                $singleName [] = $dstName;
                $archiveContent [$cat][T_ENTRIES][] = [T_HASH => $link ['hash'], T_FILENAME => $dstName, T_CRYPT_KEY => $cryptKey, T_CRYPTED => $link ['crypted']];
            }

    // build zip
    $dirname = M_NO_SENDER.(isset ($archiveInfo [T_TIME]) && !empty ($archiveInfo [T_TIME])) ?
             $archiveInfo [T_TIME] : date ("Y-m-d-H:i:s");
    $dirname = str_replace (":", "_", $dirname);
    $tmpFileName = tempnam (sys_get_temp_dir (), $dirname."-");
    $zip = new ZipArchive;
    if (!$zip)
        returnError (E_CREATE_ZIP);
    if ($zip->open ($tmpFileName.T_ZIP_EXT, ZipArchive::CREATE) !== TRUE)
        returnError (E_OPEN_ZIP);

    // create info
    if ($modif) {
        $info = '';
        if (isset ($archiveContent [T_NOT_FOUND]))
            $info .= $archiveContent [T_NOT_FOUND]. ($archiveContent [T_NOT_FOUND] ? M_FILE_NOT_FOUND : M_FILES_NOT_FOUND).NL;
        if (isset ($archiveContent [T_BAD_PASW]))
            $info .= M_BAD_KEY. $archiveContent [T_BAD_PASW]. ($archiveContent [T_BAD_PASW] ? M_FILE : M_FILES).NL;
        if (isset ($archiveContent [T_RENAME]))
            $info .= $archiveContent [T_RENAME]. ($archiveContent [T_RENAME] ? M_FILE_RENAMED : M_FILES_RENAMED).NL;
        $zip->addFromString ($dirname.T_WARNING_FILENAME, $info);
    }
    foreach ([T_OLD, T_NEW] as $cat)
        if (isset ($archiveContent [$cat])) {
            $subdir = $dirname . "-".($cat == T_NEW ? M_NEW_ATTACHEMENT_DIRNAME : M_OLD_ATTACHEMENT_DIRNAME);
            foreach ($archiveContent [$cat][T_ENTRIES] as $entry) {
                $p = s2p ($entry [T_HASH]);
                if ($entry [T_CRYPTED]) {
                    $m = mcrypt_module_open ('rijndael-256', '', 'ofb', '');
                    $md5_key = md5 ($entry [T_CRYPT_KEY]);
                    $iv = jirafeau_crypt_create_iv ($md5Key, mcrypt_enc_get_iv_size ($m));
                    mcrypt_generic_init ($m, $md5Key, $iv);
                    $r = fopen (VAR_FILES . $p . $entry [T_HASH], 'r');
                    $content = "";
                    while (!feof ($r)) {
                        $dec = mdecrypt_generic ($m, fread ($r, 1024));
                        $content .= $dec;
                        ob_flush ();
                    }
                    fclose ($r);
                    $zip->addFromString ($subdir."/".$entry [T_FILENAME], $content);
                    mcrypt_generic_deinit ($m);
                    mcrypt_module_close ($m);
                    continue;
                }
                $zip->addFile (VAR_FILES.$p.$entry [T_HASH], $subdir."/".$entry [T_FILENAME]);
            }
        }
    $zip->close ();

    
    if (false) {
        // log
        $message .= print_r ($archiveInfo, 1);
        $message .= print_r ($archiveContent, 1);

        header ('HTTP/1.0 200 OK');
        header ('Content-Length: ' . strlen ($message));
        header ('Content-Type: text/plain');
        echo $message;
        exit;
    }

    if (!is_file ($tmpFileName.T_ZIP_EXT,))
        returnError (E_OPEN_ZIP);

    header ("Content-Type: application/zip");
    header ('Content-Disposition: filename="'.$dirname.'.zip"');
    $r = fopen ($tmpFileName.".zip", 'r');
    while (!feof ($r)) {
        print fread ($r, 1024);
        ob_flush ();
    }
    fclose ($r);

    unlink ($tmpFileName.".zip");
    unlink ($tmpFileName);
    exit;    
}

// ========================================
// form

if (! ($sender && $token && $token == $refToken &&
       (getLoggedToken ($sender) || (getTimeToken ($sender) >= strtotime (TOKEN_LOGIN_LIMIT))) &&
       (getCreateToken ($sender) >= strtotime (TOKEN_LOGOUT_LIMIT)))) {
    // XXX temps de connexion
    displayHeadPage ("Gestion des pièces jointes");
    if ($senderError)
        $message .= '<div class="message-error">'.M_BAD_SENDER_NAME.'</div>'.NL;
    else if (($token && !$refToken) || !getLoggedToken ($sender))
        $message .= '<div class="message-error">'.M_TOO_LONG_BEFORE_LOGGED.'</div>'.NL;
    else if ($token && $token != $refToken)
        $message .= '<div class="message-error">'.M_BAD_TOKEN.'</div>'.NL;
    else if (getCreateToken ($sender) < strtotime (TOKEN_LOGOUT_LIMIT))
        $message .=  '<div class="message-error">'.M_TOO_LONG_LOGGED.'</div>'.NL;

    if ($message)
    echo $message;

    displayLogin (M_LOGIN_CONFIG);
    displayFootPage ();
    exit;
}

if (!getLoggedToken ($sender))
    setLoggedToken ($sender, $token);
else
    touch (VAR_TOKENS.$sender);

// ========================================
// sender OK, token OK
// ========================================

$admin = isAdmin ($sender);

// delete
if (isset ($_REQUEST [A_DELETE])) {
    if (!preg_match ('/[0-9a-zA-Z_-]+$/', $_REQUEST [A_DELETE]))
        returnError (t ('FILE_404'));
    deleteAction ($_REQUEST [A_DELETE]);
}

// logout
if ($doLogout || (isset ($_REQUEST [A_ACTION]) && $_REQUEST [A_ACTION] == T_LOGOUT)) {
    rmToken ($sender);
    displayHeadPage ("Gestion des pièces jointes");
    echo str_replace (["___SENDER___", "___ADMIN___", "___DATE___"],
                      [$sender, ($admin ? " (admin)" : ""), jirafeau_get_datetimefield (time ())],
                      M_WELCOME);
    if ($message)
        echo '<div class="message-success">Info : '.$message.'</div>';
    echo M_LOGOUT;
    displayFootPage ();
    exit;
}

if (isset ($_REQUEST [A_ACTION])) {
    // change track
    switch (true) {
    case preg_match ("/^".A_MODE."(".implode ("|", array_keys ($modeText)).")$/i", $_REQUEST [A_ACTION], $matches):
        setSenderMode ($sender, $matches [1]);
        $message .= 'Votre mode &agrave; &eacute;t&eacute; mise &agrave; jour.';
        break;
    case preg_match ("/^".A_RECORD."(on|off)$/i", $_REQUEST [A_ACTION], $matches):
        if ($matches [1] == "on")
            setSenderTrack ($sender);
        else
            rmSenderTrack ($sender);
        $message .= "Votre suivi &agrave; &eacute;t&eacute; mise &agrave; jour.";
        break;
    case preg_match ("/^".A_PERIOD."(".implode ("|", array_keys ($periodText)).")$/i", $_REQUEST [A_ACTION], $matches):
        setSenderPeriod ($sender, $matches [1]);
        $message .= "Votre p&eacute;riode &agrave; &eacute;t&eacute; mise &agrave; jour.";
        break;
    case preg_match ("/^".A_LANG."(".implode ("|", array_keys ($langText)).")$/i", $_REQUEST [A_ACTION], $matches):
        setSenderLang ($sender, $matches [1]);
        $message .= "Votre lang &agrave; &eacute;t&eacute; mise &agrave; jour.";
        break;
    }
}

// list
$archives = [];
$stack = array (VAR_LINKS);
while ( ($d = array_shift ($stack)) && $d != null) {
    $dir = scandir ($d);
    foreach ($dir as $dirName) {
        if (strcmp ($dirName, '.') == 0 || strcmp ($dirName, '..') == 0 ||
            preg_match ('/\.tmp/i', "$dirName")) {
            continue;
        }
        if (is_dir ($d . $dirName)) {
            /* Push new found directory. */
            $stack [] = $d . $dirName . '/';
            continue;
        }
        /* Read link informations. */
        $l = jirafeau_get_link ($dirName);
        if (!count ($l))
            continue;
        if (!isKazArchive ($l))
            continue;
        $archiveInfo = readArchiveFromLink ($l);
        if ($sender != $archiveInfo [T_SENDER])
            continue;
        $archiveInfo ['link'] = $dirName;
        $archiveInfo ['key'] = $l ['key'];
        $archiveInfo ['maxtime'] = $l ['time'];
        $archiveInfo ['hash'] = $l ['hash'];
        $archives [] = $archiveInfo;
    }
}
displayHeadPage ("Gestion des pièces jointes");
echo str_replace (["___SENDER___", "___ADMIN___", "___DATE___"],
                  [$sender, ($admin ? " (admin)" : ""), jirafeau_get_datetimefield (time ())],
                  M_WELCOME);
if ($message)
    echo '<div class="message-success">Info : '.$message.'</div>';
echo '<script type="text/javascript">';
?>
function getURI (uri, params) {
    var form = document.createElement ('form');
    form.setAttribute ('method', 'post');
    form.setAttribute ('action', "<?php echo $urlBase; ?>"+uri);
    var hiddenField = document.createElement ('input');
    for (var key in params) {
        if (params.hasOwnProperty (key)) {
            var hiddenField = document.createElement ('input');
            hiddenField.setAttribute ('type', 'hidden');
            hiddenField.setAttribute ('name', key);
            hiddenField.setAttribute ('value', params [key]);
            form.appendChild (hiddenField);
        }
        document.body.appendChild (form);
        form.submit ();
    }
}
function getKazArchive (l, k) {
    getURI ("<?php echo $_SERVER ['SCRIPT_NAME']; ?>", {<?php echo A_GET; ?>:l+'~'+k});
}
function getKazFile (l, k) {
    window.location.href = "<?php echo $urlBase; ?>/f.php?h="+l+"&k="+k;
}
function showLink (l, k) {
    alert ("<?php echo $urlBase;?>/f.php?h="+l+"&k="+k);
}
function ajaxUpdate (period, hash) {
    var xhr = new XMLHttpRequest ();
    xhr.open ("POST", "<?php echo $_SERVER ['SCRIPT_NAME']; ?>", true);
    xhr.setRequestHeader ("Content-Type", "application/x-www-form-urlencoded");
    xhr.onreadystatechange = function () {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            getURI ("<?php echo $_SERVER ['SCRIPT_NAME']; ?>", {<?php echo A_SENDER.": '".$sender."', ".A_TOKEN.": '".$token."'"; ?>});
        }
    }
       xhr.send ("<?php echo A_UPDATE; ?>="+period+"&<?php echo A_HASH; ?>="+hash);
}
function rmFile (f) {
    if (!confirm ("Les suppressions sont definitives. Voulez-vous continuer ?"))
        return;
    getURI ("<?php echo $_SERVER ['SCRIPT_NAME']; ?>", {<?php echo A_DELETE; ?>: f, <?php echo A_SENDER.": '".$sender."', ".A_TOKEN.": '".$token."'"; ?>});
}
</script>

<style type="text/css"><!-- a {text-decoration: none;}
div.frame {border: 1px; border-style: solid; padding: 1em; margin: 1em;}
[data-tooltip]:before {position : absolute; content : attr(data-tooltip); opacity : 0; background: yellow; padding: 10px; marging: 1em; transform: translate(10px, 10px);}
[data-tooltip]:hover:before {opacity : 1;}
[data-tooltip]:not([data-tooltip-persistent]):before {pointer-events: none;}
--></style>
<?php
$defaultChecked = [];
$defaultChecked [getSenderMode ($sender)] = ' selected="selected"';
$defaultChecked [isSenderTrack ($sender) ? "on" : "off"] = ' checked="checked"';
$defaultChecked [getSenderPeriod ($sender)] = ' selected="selected"';
$defaultChecked [getSenderLang ($sender)] = ' selected="selected"';
echo
    '<form method="post">'.
    'Je veux que mes futurs envois soient d&eacute;pollu&eacute;s en pla&ccedil;ant les liens de t&eacute;l&eacute;chargements '.
    '<select name="'.A_ACTION.'" style="width: auto !important;">';
foreach ($modeText as $item => $text)
    echo '  <option value="'.A_MODE.$item.'"'.$defaultChecked [$item].'>'.$text.'</option>';
echo
    '</select> '.
    '<button type="submit">'."valider".'</button>'.
    '</form>'.
    '<form method="post">'.
    'Je veux que Kaz suive tous mes futurs envois: '.
    '<input type="hidden" name="'.A_SENDER.'" value="'.$sender.'"/>'.
    '<input type="hidden" name="'.A_TOKEN.'" value="'.$token.'"/>';
foreach ($trackText as $item => $text)
    echo '<input type="radio" name="'.A_ACTION.'" value="'.A_RECORD.$item.'"'.$defaultChecked [$item].'>'.$text.' ';
echo
    '<button type="submit">'."valider".'</button>'.
    '</form>'.
    '<form method="post">'.
    'Je veux que mes futurs envois soient accessibles pendant au moins un&middot;e '.
    '<select name="'.A_ACTION.'" style="width: auto !important;">';
foreach ($periodText as $item => $text)
    echo '  <option value="'.A_PERIOD.$item.'"'.$defaultChecked [$item].'>'.$text.'</option>';
echo
    '</select> '.
    '<button type="submit">'."valider".'</button>'.
    '</form>'.
    '<form method="post">'.
    'Je préfère envoyer mes messages en langue '.
    '<select name="'.A_ACTION.'" style="width: auto !important;">';
foreach ($langText as $item => $text)
    echo '  <option value="'.A_LANG.$item.'"'.$defaultChecked [$item].'>'.$text.'</option>';
echo
    '</select> '.
    '<button type="submit">'."valider".'</button>'.
    '</form>'.
    '<form method="post">'.
    'Je veux '.
    '<input type="hidden" name="'.A_SENDER.'" value="'.$sender.'"/>'.
    '<input type="hidden" name="'.A_TOKEN.'" value="'.$token.'"/>'.
    '<button type="submit">'.M_REFRESH.'</button>'.
    ' la page.</form>';

$userSise = 0;
$userTab = [];
if ($archives) {
    foreach ($archives as $archiveInfo) {
        $contentSize = 0;
        $archContent = '';
        foreach ([T_NEW, T_OLD] as $cat) {
            $liStyle = $cat == T_NEW ? "font-weight: bold;" : "font-style: italic;";
            if (isset ($archiveInfo [$cat]))
                foreach ($archiveInfo [$cat] as [$linkName, $cryptKey]) {
                    $link = jirafeau_get_link ($linkName);
                    if (count ($link) == 0)
                        continue;
                    if ($cat == T_NEW )
                        $contentSize += $link ['file_size'];
                    $lf = $linkName;
                    $kf = $link ['key'];
                    $archContent .=
                                 '<li style="list-style:none; '.$liStyle.'">'.
                                 '<a data-tooltip="voir" href="javascript:getKazFile (\''.$lf.'\', \''.$kf.'\');"> &#128065; </a>'.
                                 '<a data-tooltip="voir le lien" href="javascript:showLink (\''.$lf.'\', \''.$kf.'\');"> &#128279; </a>';
                    // foreach ($periodButton as $item => $bt)
                    //     echo '<a data-tooltip="'.$bt[1].'" href="javascript:ajaxUpdate (\''.$lf.'\', \''.$kf.'\');"> '.$bt[0].' </a>';
                    $archContent .=
                                 jirafeau_escape ($link ['file_name']).
                                 ' ('.jirafeau_escape ($link ['mime_type']).
                                 ' '.jirafeau_human_size ($link ['file_size']).')';
                    if ($cat == T_NEW)
                        $archContent .=
                                     '<a data-tooltip="supprimer" href="javascript:rmFile (\''.$lf.'\');"> &#9850; </a>';
                    $archContent .=
                                 '</li>';
                }
        }
        $archEntry =
                   '<div class="frame" width="100%" >';
        $la = $archiveInfo ['link'];
        $ka = $archiveInfo ['key'];
        $archEntry .= 
                   '<a data-tooltip="voir" href="javascript:getKazArchive (\''.$la.'\', \''.$ka.'\');"> &#128065; </a>'.
                   '<a data-tooltip="voir le lien" href="javascript:showLink (\''.$la.'\', \''.$ka.'\');"> &#128279; </a>';
        foreach ($periodButton as $item => $bt)
            $archEntry .=
                       '<a data-tooltip="'.$bt[1].'" href="javascript:ajaxUpdate (\''.$item.'\', \''.$la.'\');"> '.$bt[0].' </a>';
        $archEntry .=
                   '<strong>'.$archiveInfo [T_TIME].' ('.jirafeau_human_size ($contentSize).')</strong>'.
                   '<a data-tooltip="supprimer tous" href="javascript:rmFile (\''.$la.'\');"> &#9850; </a>'.
                   '<br/>=> '. ($archiveInfo ['maxtime'] == -1 ? '∞' : jirafeau_get_datetimefield ($archiveInfo ['maxtime'])).'<ul>'.
                   $archContent.
                   '</div>';
        $userSise += $contentSize;
        $userTab [getTimeFile ($archiveInfo ['hash'])] = $archEntry; 
    }
    ksort ($userTab);
}

echo
    '<span>Votre compte occupe <b>'.jirafeau_human_size ($userSise).'</b>.</span>'.
    '<form method="post">'.
    'Je veux me '.
    '<input type="hidden" name="'.A_ACTION.'" value="'.T_LOGOUT.'" />'.
    '<input type="hidden" name="'.A_SENDER.'" value="'.$sender.'"/>'.
    '<input type="hidden" name="'.A_TOKEN.'" value="'.$token.'"/>'.
    '<button type="submit">'.M_LOGOUT.'</button>'.
    '.</form>';
if (count ($userTab)) {
    foreach ($userTab as $time => $entry)
        echo $entry;
} else
    echo
        "<span>Il n'y a aucune information vous concernant.</span>";

displayFootPage ();
exit;

// ========================================
