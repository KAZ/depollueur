<?php
/*
 *  Kaz addon (see https://git.kaz.bzh/KAZ/depollueur for information)
 *  only upload file for restricted acces purpose
 *  version : 2.24 (2025-01-26)
*/

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;
require 'vendor/autoload.php';

define ('JIRAFEAU_ROOT', dirname (__FILE__) . '/');

require (JIRAFEAU_ROOT . 'lib/settings.php');
require (JIRAFEAU_ROOT . 'lib/functions.php');
require (JIRAFEAU_ROOT . 'lib/lang.php');

// ========================================
function period2seconds ($periodName) {
    if (!$periodName)
        return JIRAFEAU_MONTH;
    switch ($periodName) {
    case 'minute':
        return JIRAFEAU_MINUTE;
        break;
    case 'hour':
        return JIRAFEAU_HOUR;
        break;
    case 'day':
        return JIRAFEAU_DAY;
        break;
    case 'week':
        return JIRAFEAU_WEEK;
        break;
    case 'month':
        return JIRAFEAU_MONTH;
        break;
    case 'quarter':
        return JIRAFEAU_QUARTER;
        break;
    case 'semester':
        return JIRAFEAU_SEMESTER;
        break;
    case 'year':
        return JIRAFEAU_YEAR;
        break;
    default:
        returnError (t ('ERR_OCC') . ' (periodName)');
    }
}

// ========================================
function returnError ($msg) {
    require (JIRAFEAU_ROOT.'lib/template/header.php');
    echo '<div class="error"><p>' . $msg . '</p></div>';
    require (JIRAFEAU_ROOT.'lib/template/footer.php');
    exit;
}

if (! isset ($_FILES ['file'])) {
    // XXX t ("OnlyUpload")
    returnError ("Only upload file");
}

// ========================================
$maxtime = time ()+period2seconds ($_REQUEST ['time']);
$key = isset ($_REQUEST ['key']) ? $_REQUEST ['key'] : '';
$ip = $_SERVER ['HTTP_X_REAL_IP']; // XXX
$res = jirafeau_upload (
    $_FILES['file'],
    isset ($_POST ['one_time_download']),
    $key,
    $maxtime,
    $ip,
    $cfg['enable_crypt'],
    $cfg['link_name_length'],
    $cfg['file_hash']
);
if (! count ($res ['error']) || $res['error']['has_error'])
    $content = 'Error 6 ' . $res['error']['why'];
else
    $content = $res ['link'].NL.$res ['delete_link'].NL;
header ('HTTP/1.0 200 OK');
header ('Content-Length: ' . strlen ($content));
header ('Content-Type: text/plain');
echo $content;
exit;    

// ========================================
