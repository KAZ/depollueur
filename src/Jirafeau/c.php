<?php
/*
 *  Kaz addon (see https://git.kaz.bzh/KAZ/depollueur for information)
 *  foward dowload to NextCloud API

 c.php => config
 c.php?s="sender"&t="token"
 c.php?a={login|logout|forget_me} => edition/lecture
 c.php?a={add_url, del_url, up_url, down_url} => manage list url
 c.php?h=l&k=k => file
 c.php?g=l~k => zip
*/

require ("lib/attach-setup.php");

// ========================================
function setUserProfile ($profile, $listUrl) {
    if (!$profile)
        return;
    if (!file_exists (VAR_CLOUD))
        mkdir (VAR_CLOUD, 0755);
    if (empty ($listUrl)) {
        rmUserProfile ($profile);
        return;
    }
    return file_put_contents (VAR_CLOUD.$profile, json_encode ($listUrl, JSON_PRETTY_PRINT)) !== false;
}
function rmUserProfile ($profile) {
    if (!$profile)
        return;
    if (file_exists (VAR_CLOUD.$profile))
        unlink (VAR_CLOUD.$profile);
}

/**
 * Récupère les données Nextcloud d'un utilisateur.
 *
 * @param string $profile Email de l'utilisateur.
 * @return array Données Nextcloud de l'utilisateur.
 */
function getUserProfile ($profile) {
    $listUrl = [];
    if ($profile && file_exists (VAR_CLOUD.$profile))
        $listUrl = json_decode (file_get_contents (VAR_CLOUD.$profile), true);
    return $listUrl;
}

// ==============================================
// Fonctions utilitaires
// ==============================================
$name = '';
$url = '';
function manageUrl () {
    global $sender, $message, $name, $url;
    if (!isset ($_REQUEST [A_ACTION]))
        return;
    if (!in_array ($_REQUEST [A_ACTION], array (T_ADD_URL, T_DEL_URL, T_UP_URL, T_DOWN_URL) ))
        return;
    $listUrl = getUserProfile  ($sender);
    $name = filter_var ($_REQUEST [A_NAME], FILTER_SANITIZE_STRING);
    if (empty ($name)) {
        $message .= '<div class="message-error">nom vide</div>'.NL;
        return;
    }
    switch ($_REQUEST [A_ACTION]) {
    case T_ADD_URL:
        $url = rtrim (filter_var ($_REQUEST [A_URL], FILTER_VALIDATE_URL), '/');
        if (empty ($url)) {
            $message .= '<div class="message-error">url vide</div>'.NL;
            return;
        }
        $listUrl ["urls"][$name] = $url;
        $message .= '<div class="message-success">'.M_URL_ADDED.'</div>'.NL;
        break;
        // XXX simplifier
    case T_DEL_URL:
    case T_UP_URL:
    case T_DOWN_URL:
        if (!(isset ($listUrl ["urls"]) && count ($listUrl ["urls"]) && isset ($listUrl ["urls"][$name])))
            return;
        $pos = array_search ($name, array_keys ($listUrl ["urls"]));
        $url = $listUrl ["urls"][$name];
        unset ($listUrl ["urls"][$name]);
        switch ($_REQUEST [A_ACTION]) {
        case T_DEL_URL:
            $message .= '<div class="message-success">.M_URL_DELETED.</div>'.NL;
            break;
        case T_UP_URL:
            $pos = max ($pos-1, 0);
            break;
        case T_DOWN_URL:
            $pos = min ($pos+1, count ($listUrl ["urls"]));
            break;
        }
        if ($_REQUEST [A_ACTION] != T_DEL_URL)
            $listUrl ["urls"] = array_merge (
                array_slice ($listUrl ["urls"], 0, $pos),
                array ($name => $url),
                array_slice ($listUrl["urls"], $pos)
            );
        break;
    }
    $name = $url = '';
    setUserProfile ($sender, $listUrl);
}

// ==============================================
// main
// ==============================================
// XXX
    session_start ();

//phpinfo ();
displayHeadPage ($title);
echo '<div class="block-info">'.M_SWITCH_WELCOME.'</div>'.NL;

// logout
if (isset ($_REQUEST [A_ACTION]) && $_REQUEST [A_ACTION] == T_LOGOUT) {
    $message .= '<div class="message-success">'.M_LOGOUT_TOKEN.'</div>'.NL;
    if ($sender)
        rmToken ($sender);
    unset ($token);
}

$logged = false;
if (! (!empty ($sender) && !empty ($token) && $token == $refToken &&
       (getLoggedToken ($sender) || (getTimeToken ($sender) >= strtotime (TOKEN_LOGIN_LIMIT))) &&
       (getCreateToken ($sender) >= strtotime (TOKEN_LOGOUT_LIMIT)))) {
    if ($senderError)
        $message .= '<div class="message-error">'.M_BAD_SENDER_NAME.'</div>'.NL;
    else if (($token && !$refToken) || !getLoggedToken ($sender))
        //$message .= '<div class="message-error">'.M_TOO_LONG_BEFORE_LOGGED.' '.$token.'</div>'.NL;
        ;
    else if ($token && $token != $refToken)
        $message .= '<div class="message-error">'.M_BAD_TOKEN.'</div>'.NL;
    else if (getCreateToken ($sender) < strtotime (TOKEN_LOGOUT_LIMIT)) 
        $message .= '<div class="message-error">'.M_TOO_LONG_LOGGED.'</div>'.NL;
    //$message .= '<div class="message-error">'.M_TIMEOUT_TOKEN.'</div>'.NL;
    unset ($token);
    cleanToken ();
} else {
    $logged = true;
    if (!getLoggedToken ($sender))
        setLoggedToken ($sender, $token);
    else
        touch (VAR_TOKENS.$sender);
    $_SESSION [T_PROFILE] = $sender;
}

// gestion list url
if ($logged) {
    manageUrl ();
}

$profile='';
if (isset ($_SESSION [T_PROFILE]))
    $profile = $_SESSION [T_PROFILE];

// forget
if (isset ($_REQUEST [A_ACTION]) && $_REQUEST [A_ACTION] == T_FORGETME) {
    $message .= '<div class="message-success">'.M_CLOSE_PROFILE.'</div>'.NL;
    unset ($_SESSION [T_PROFILE]);
    unset ($profile);
}

if (isset ($_REQUEST [A_HASH]))
    $message .= '<div class="message-error">Version exp&eacute;rimentale. Ne fonctionne actuellement que pour les archives.</div>'.NL;

if ($message)
    echo $message;

if (isset ($_REQUEST [A_GET]) || isset ($_REQUEST [A_HASH])) {
    $query = $urlBase;
    $cloud_app = $CLOUD_APP;
    if (isset ($_REQUEST [A_GET])) {
        $query .= '/a.php?'.http_build_query ([
            A_GET => $_REQUEST [A_GET]
        ]);
        $cloud_app .= $CLOUD_SEND_ZIP;
    } else if (isset ($_REQUEST [A_HASH])) {
        $query .= '/f.php?'.http_build_query ([
            'd' => $_REQUEST ['d'],
            A_HASH => $_REQUEST [A_HASH],
            'k' => $_REQUEST ['k']
        ]);
        $cloud_app .= $CLOUD_SEND_FILE;
    }
    // XXX revoir pour mettre ou non KAZ.
    $listUrl = '';
    if ($profile)
        $listUrl = getUserProfile  ($profile);
    displayListUrl ($cloud_app.urlencode ($query), $listUrl);
    //displayListUrl ($cloud_app.$query, $listUrl);
} else
    echo '<div class="block"><div class="block-info">'.M_SWITCH_NO_FILE.'</div></div>'.NL;

if (!($logged || empty ($profile))) {
    displayProfileName (M_SWITCH_FORGET);
}

if ($logged) {
    // Affiche menu organisation URL
    $listUrl = getUserProfile  ($sender);
    displayFormProfile ($listUrl);
    displayLogout (M_SWITCH_LOGOUT);
} else {
    displayLogin (M_SWITCH_LOGIN);
}
displayFootPage ();
