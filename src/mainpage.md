**Dépollueur**

Le dépollueur consiste à extraire les pièces jointes pour les remplacer par des liens de téléchargement temporaire.

Le logiciel est donc très fortement lié à un logiciel de dépôt de fichier.
Le système de dépôt de proposé par Jirafeau à l'avantage de gérer les liens interne en fonction de la signature du contenu (hash).
Cela offre plusieurs avantages. les fichiers de même contenu sont factorisés sans que les utilisateurs en aient consciences.
L'intégrité des données est inhérente à ce mécanisme, puisque un lien ne va pas faire référence à un autre contenu dans le dépôt.

A partir du moment où notre dépollueur à une action sur les messages, cela à des conséquences sur l’utilisation d'outils cryptographiques.
Le chiffrement de pièces jointes a peu d'incidence. L'information chiffrée se trouve stocké sur un serveur au lieu d'un ordinateur personnel (voir pire un serveur des GAFAM).
Ici, l'avantage est que l'information sera détruite au bout d'un temps spécifié.

La signature, empêche le dépollueur d'intervenir.
Dans le cas où l'on souhaite signer ses messages, il faut au préalable déposer ses pièces jointes (hors GAFAM) et faire référence dans le message aux liens de téléchargement.



Main Programme:
  * [eMailShrinker](eMailShrinker_8cpp.html)

Main classes:
  * [MainAttachment](classkaz_1_1MainAttachment.html)
  * [Attachment](classkaz_1_1Attachment.html)
  
