##########################################################################
# Copyright KAZ 2021							 #
# 									 #
# contact (at) kaz.bzh							 #
# 									 #
# This software is a filter to shrink email by attachment extraction.	 #
# 									 #
# This software is governed by the CeCILL-B license under French law and #
# abiding by  the rules of distribution  of free software. You  can use, #
# modify  and/or  redistribute  the  software under  the  terms  of  the #
# CeCILL-B license as circulated by CEA, CNRS and INRIA at the following #
# URL "http://www.cecill.info".						 #
# 									 #
# As a counterpart to the access to  the source code and rights to copy, #
# modify and  redistribute granted  by the  license, users  are provided #
# only with a limited warranty and  the software's author, the holder of #
# the economic  rights, and the  successive licensors have  only limited #
# liability.								 #
# 									 #
# In this respect, the user's attention is drawn to the risks associated #
# with loading,  using, modifying  and/or developing or  reproducing the #
# software by the user in light of its specific status of free software, #
# that may  mean that  it is  complicated to  manipulate, and  that also #
# therefore means  that it  is reserved  for developers  and experienced #
# professionals having in-depth computer  knowledge. Users are therefore #
# encouraged  to load  and test  the software's  suitability as  regards #
# their  requirements  in  conditions  enabling the  security  of  their #
# systems and/or  data to  be ensured  and, more  generally, to  use and #
# operate it in the same conditions as regards security.		 #
# 									 #
# The fact that  you are presently reading this means  that you have had #
# knowledge of the CeCILL-B license and that you accept its terms.	 #
##########################################################################

# apt-get install g++ make libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libssl-dev

## DIR #################################
SRC_DIR   = ./src
TST_DIR   = $(SRC_DIR)/test
CPP_DIR   = $(SRC_DIR)/cpp
HPP_DIR   = $(SRC_DIR)/include
BLD_DIR   = build
OUT_DIR   = $(BLD_DIR)/out
LIB_DIR   = $(BLD_DIR)/lib
OBJ_DIR   = $(BLD_DIR)/obj

EMS_PRG = eMailShrinker
EMS_MOD = eMailShrinker EmbeddedData Attachment MainAttachment SizeArg kazDebug kazMisc
EMS_SRC = $(patsubst %, $(CPP_DIR)/%.cpp, $(EMS_MOD))
EMS_OBJ = $(patsubst %, $(OBJ_DIR)/%.o, $(EMS_MOD))
EMS_OUT = $(patsubst %, $(OUT_DIR)/%, $(EMS_PRG))

SRV_PRG = server
SRV_MOD = server kazDebug kazMisc
SRV_SRC = $(patsubst %, $(CPP_DIR)/%.cpp, $(SRV_MOD))
SRV_OBJ = $(patsubst %, $(OBJ_DIR)/%.o, $(SRV_MOD))
SRV_OUT = $(patsubst %, $(OUT_DIR)/%, $(SRV_PRG))

TSRV_PRG = testServerRW
TSRV_MOD = testServerRW kazDebug
TSRV_SRC = $(patsubst %, $(CPP_DIR)/%.cpp, $(TSRV_MOD))
TSRV_OBJ = $(patsubst %, $(OBJ_DIR)/%.o, $(TSRV_MOD))
TSRV_OUT = $(patsubst %, $(OUT_DIR)/%, $(TSRV_PRG))

## FLAGS ###############################

#DFLAGS = -O2 -DDISABLE_LOG
DFLAGS = -g -Wall

IFLAGS = $(DFLAGS) -MMD -I$(HPP_DIR) -std=c++11

LFLAGS = -L$(LIB_DIR) -lboost_system -lboost_program_options -lboost_filesystem -lcurl -lcrypto -lstdc++
CC = g++

## RULES ###############################
$(OBJ_DIR)/%.o: $(SRC_DIR)/*/%.cpp
	$(CC) $< $(IFLAGS) -cpp -c -o $@

## ENTRIES #############################
all:	init $(EMS_PRG) $(SRV_PRG) $(TSRV_PRG) doc

$(EMS_PRG): $(EMS_OUT)

$(EMS_OUT): $(EMS_OBJ)
	$(CC) $(EMS_OBJ) $(IFLAGS) -cpp -L$(LIB_DIR) $(LFLAGS) -o $@

$(SRV_PRG): $(SRV_OUT)

$(SRV_OUT): $(SRV_OBJ)
	$(CC) $(SRV_OBJ) $(IFLAGS) -cpp -L$(LIB_DIR) $(LFLAGS) -o $@

$(TSRV_PRG): $(TSRV_OUT)

$(TSRV_OUT): $(TSRV_OBJ)
	$(CC) $(TSRV_OBJ) $(IFLAGS) -cpp -L$(LIB_DIR) $(LFLAGS) -o $@

image:
	docker build -t depollueur . -f ./Dockerfile

doc:
	doxygen src/Doxyfile

init:
	mkdir -p $(OUT_DIR) $(OBJ_DIR) $(LIB_DIR)

clean:
	find . -type f '(' -name '#*' -o -name '*~' ')' -print -exec rm -f '{}' \;

wipe: clean
	-rm -f src/*.tcpp src/*/*.tcpp
	-rm -rf $(OUT_DIR) $(LIB_DIR) $(BLD_DIR)

## DEPENDS #############################
ALL_OUT = $(EMS_PRG) $(SRV_PRG) $(TSRV_PRG)
ALL_OBJ = $(EMS_OBJ) $(SRV_OBJ) $(TSRV_OBJ)

DEPENDS = ${ALL_OUT:=.d} ${ALL_OBJ:.o=.d}
-include ${DEPENDS}

########################################
