# docker build -t depollueur . -f ./docker/Dockerfile
# two stage building
# 1) install compiler and compile filter
# 2) copy filter and install postfix
# Doxkerfile patern from https://vsupalov.com/cache-docker-build-dependencies-without-volume-mounting/
FROM debian as intermediate_depollueur

########################################
RUN apt-get update && apt-get -y autoremove
RUN apt-get -y install locales locales-all
RUN sed -i '/fr_FR.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
RUN update-locale LANG=fr_FR.UTF-8

RUN apt-get -y install emacs elpa-php-mode apg
RUN apt-get -y install apg dos2unix

# creation du user filter,son repertoire home, copie des fichiers
RUN mkdir /home/filter ; useradd -d /home/filter filter ; chown filter /home/filter
########## >>> ce qui suit va être jetté
RUN apt-get install -y --fix-missing doxygen git \
    build-essential make g++ libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libcurl4-gnutls-dev libssl-dev
WORKDIR /home/
RUN git clone https://git.kaz.bzh/KAZ/depollueur.git
WORKDIR /home/depollueur/
RUN make
RUN cp build/out/* /home/filter/
RUN cp src/bash/* /home/filter/
########## <<< on ne garde que le répertoire ci-dessous

##########################################################################
# ###################################################################### #
# #								       # #
# # On jette tous ce qui est au-dessus pour ne garder que /home/filter # #
# #								       # #
# ###################################################################### #
##########################################################################

FROM debian

########################################
RUN apt-get update && apt-get -y autoremove
RUN apt-get -y install locales locales-all
RUN sed -i '/fr_FR.UTF-8/s/^# //g' /etc/locale.gen && locale-gen
ENV LC_ALL fr_FR.UTF-8
ENV LANG fr_FR.UTF-8
ENV LANGUAGE fr_FR:fr
RUN update-locale LANG=fr_FR.UTF-8

RUN apt-get -y install emacs elpa-php-mode apg
RUN apt-get -y install apg dos2unix

# creation du user filter,son repertoire home, copie des fichiers
RUN mkdir /home/filter ; useradd -d /home/filter filter ; chown filter /home/filter
########## >>> On fait excatement la même chose que la première fois *
RUN apt-get -y install libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libcurl4-gnutls-dev
##########     pour profiter du cahe des couche de docker
COPY --from=intermediate_depollueur /home/filter /home/filter
########## <<< mais cette fois on n'installe pas le compilo
RUN chown filter /home/filter/*; chmod a+rx /home/filter/*

# pour le confort : modif du .bashrc de root
RUN sed -i 's/# alias/alias/g' /root/.bashrc

RUN mkdir /var/log/mail ; chmod a+wrx /var/log/mail

EXPOSE 8080
VOLUME [ "/var/log/mail" ]
ENTRYPOINT [ "/home/filter/server" ]

#HEALTHCHECK --interval=10s --timeout=5s --start-period=20s \
#  CMD XXX || exit 1
