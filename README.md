# Dépollueur de courriel

## Contenu

```
depollueur/
├── LICENCE
├── README.md
└── src
    ├── bash
    │   └── filter.sh
    ├── cpp
    │   ├── Attachment.cpp
    │   ├── eMailShrinker.cpp
    │   ├── EmbeddedData.cpp
    │   ├── kazDebug.cpp
    │   ├── kazMisc.cpp
    │   ├── MainAttachment.cpp
    │   └── SizeArg.cpp
    ├── include
    │   ├── Attachment.hpp
    │   ├── EmbeddedData.hpp
    │   ├── kazDebug.hpp
    │   ├── kazMisc.hpp
    │   ├── MainAttachment.hpp
    │   └── SizeArg.hpp
    └── Jirafeau
        ├── f.php
        └── t.php
```

## Compilation

```bash
sudo apt-get install --fix-missing build-essential make g++ libboost-program-options-dev libboost-system-dev libboost-filesystem-dev libcurl4-gnutls-dev libssl-dev doxygen dos2unix

git clone https://git.kaz.bzh/KAZ/depollueur.git
# or for contributors :
# git clone git+ssh://git@git.kaz.bzh:2202/KAZ/depollueur.git

make -j $(nproc)
```

##Astuces


Pour la mise au point dans Kaz, il faut bloquer la mise à jour automatique avec :

``` bash
touch /kaz/git/depollueur/dontupdage
/kaz/bin/container.sh stop jirafeau postfix; docker system prune; /kaz/bin/install.sh jirafeau postfix
```

